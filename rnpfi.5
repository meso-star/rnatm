.\" Copyright (C) 2022, 2023 Centre National de la Recherche Scientifique
.\" Copyright (C) 2022, 2023 Institut Pierre-Simon Laplace
.\" Copyright (C) 2022, 2023 Institut de Physique du Globe de Paris
.\" Copyright (C) 2022, 2023 |Méso|Star>(contact@meso-star.com)
.\" Copyright (C) 2022, 2023 Observatoire de Paris
.\" Copyright (C) 2022, 2023 Université de Reims Champagne-Ardenne
.\" Copyright (C) 2022, 2023 Université de Versaille Saint-Quentin
.\" Copyright (C) 2022, 2023 Université Paul Sabatier
.\"
.\" This program is free software: you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation, either version 3 of the License, or
.\" (at your option) any later version.
.\"
.\" This program is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with this program. If not, see <http://www.gnu.org/licenses/>.
.Dd September 26, 2023
.Dt RNPFI 5
.Os
.Sh NAME
.Nm rnpfi
.Nd Rad-Net Phase Function Indices
.Sh DESCRIPTION
.Nm
is a binary file format for storing a list of indices referring to
phase functions.
The indices are attached to a volumetric mesh which is
.Em not
described there but must be defined in a separate file, for example in an
.Xr smsh 5
file.
The number of indices and their order must therefore correspond to the
data of the mesh to which they are attached, i.e. to the nodes or cells
listed in the corresponding mesh.
.Pp
A
.Nm
file is actually a Star-Buffer file
.Pq see Xr sbuf 5 .
It starts with a header of 4 integers.
The first integer is a power of two
.Pq usually 4096
that defines the size of the memory page in bytes
.Pq Va pagesize
on which the list of indices are aligned.
By aligning data to pagesize, and depending on system requirements,
memory mapping can be used to automatically load/unload pages on demand
.Pq see Xr mmap 2 .
The second integer is the
.Va size
of the array, i.e. the number of indices stored in the list.
The two remaining integers store the memory size
.Pq 4 bytes
and the memory alignment
.Pq 4 bytes
of an index.
.Pp
Padding bytes follow the file header to align the listed indices to
.Va pagesize .
.Pp
Padding bytes are finally added at the end of the file to align its
overall size with the size of a page.
.Pp
Data are encoded with respect to the little endian bytes ordering,
i.e. least significant bytes are stored first.
.Pp
The file format is as follows:
.Bl -column (pagesize) (::=) ()
.It Ao Va rnpfi Ac Ta ::= Ta Ao Va pagesize Ac Ao Va size Ac Li 4 4
.It Ta Ta Aq Va padding
.It Ta Ta Aq Va indices
.It Ta Ta Aq Va padding
.It Ao Va pagesize Ac Ta ::= Ta Vt uint64_t
.It Ao Va size  Ac Ta ::= Ta Vt uint64_t
# Number of indices stored
.It Ao Va indices Ac Ta ::= Ta Vt uint32_t ...
.It Ao Va padding Ac Ta ::= Ta Op Vt int8_t ...
.El
.Sh SEE ALSO
.Xr mmap 2 ,
.Xr sbuf 5 ,
.Xr smsh 5
.Sh HISTORY
The
.Nm
format was first developed for the
.Xr htrdr-planeto 1
program.
