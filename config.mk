VERSION = 0.1.0
PREFIX = /usr/local

LIB_TYPE = SHARED
#LIB_TYPE = STATIC

BUILD_TYPE = RELEASE
#BUILD_TYPE = DEBUG

################################################################################
# Tools
################################################################################
AR = ar
CC = cc
LD = ld
OBJCOPY = objcopy
PKG_CONFIG = pkg-config
RANLIB = ranlib

################################################################################
# Dependencies
################################################################################
PCFLAGS_SHARED =
PCFLAGS_STATIC = --static
PCFLAGS = $(PCFLAGS_$(LIB_TYPE))

RNSF_VERSION = 0.1
RNSF_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags rnsf)
RNSF_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs rnsf)

RNSL_VERSION = 0.1
RNSL_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags rnsl)
RNSL_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs rnsl)

RSYS_VERSION = 0.14
RSYS_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags rsys)
RSYS_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs rsys)

SARS_VERSION = 0.1
SARS_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags sars)
SARS_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs sars)

SBUF_VERSION = 0.1
SBUF_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags sbuf)
SBUF_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs sbuf)

SCK_VERSION = 0.1
SCK_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags sck)
SCK_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs sck)

SMSH_VERSION = 0.1
SMSH_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags smsh)
SMSH_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs smsh)

SSF_VERSION = 0.9
SSF_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags ssf)
SSF_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs ssf)

SUVM_VERSION = 0.3
SUVM_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags suvm)
SUVM_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs suvm)

SVX_VERSION = 0.3
SVX_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags svx)
SVX_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs svx)

DPDC_CFLAGS =\
 $(RNSF_CFLAGS)\
 $(RNSL_CFLAGS)\
 $(RSYS_CFLAGS)\
 $(SARS_CFLAGS)\
 $(SBUF_CFLAGS)\
 $(SCK_CFLAGS)\
 $(SMSH_CFLAGS)\
 $(SSF_CFLAGS)\
 $(SUVM_CFLAGS)\
 $(SVX_CFLAGS)\
 -fopenmp

DPDC_LIBS =\
 $(RNSF_LIBS)\
 $(RNSL_LIBS)\
 $(RSYS_LIBS)\
 $(SARS_LIBS)\
 $(SBUF_LIBS)\
 $(SCK_LIBS)\
 $(SMSH_LIBS)\
 $(SSF_LIBS)\
 $(SUVM_LIBS)\
 $(SVX_LIBS)\
 -fopenmp\
 -lm

################################################################################
# Compilation options
################################################################################
WFLAGS =\
 -Wall\
 -Wcast-align\
 -Wconversion\
 -Wextra\
 -Wmissing-declarations\
 -Wmissing-prototypes\
 -Wshadow

# Increase the security and robustness of generated binaries
CFLAGS_HARDENED =\
 -D_FORTIFY_SOURCES=2\
 -fcf-protection=full\
 -fstack-clash-protection\
 -fstack-protector-strong

CFLAGS_COMMON =\
 -std=c89\
 -pedantic\
 -fvisibility=hidden\
 -fstrict-aliasing\
 $(CFLAGS_HARDENED)\
 $(WFLAGS)

CFLAGS_DEBUG = -g $(CFLAGS_COMMON)
CFLAGS_RELEASE = -O2 -DNDEBUG $(CFLAGS_COMMON)
CFLAGS = $(CFLAGS_$(BUILD_TYPE))

CFLAGS_SO = $(CFLAGS) -fPIC
CFLAGS_EXE = $(CFLAGS) -fPIE

################################################################################
# Linker options
################################################################################
LDFLAGS_HARDENED = -Wl,-z,relro,-z,now
LDFLAGS_DEBUG = $(LDFLAGS_HARDENED)
LDFLAGS_RELEASE = -s $(LDFLAGS_HARDENED)
LDFLAGS = $(LDFLAGS_$(BUILD_TYPE))

LDFLAGS_SO = $(LDFLAGS) -shared -Wl,--no-undefined
LDFLAGS_EXE = $(LDFLAGS) -pie

OCPFLAGS_DEBUG = --localize-hidden
OCPFLAGS_RELEASE = --localize-hidden --strip-unneeded
OCPFLAGS = $(OCPFLAGS_$(BUILD_TYPE))
