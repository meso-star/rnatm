/* Copyright (C) 2022, 2023 Centre National de la Recherche Scientifique
 * Copyright (C) 2022, 2023 Institut Pierre-Simon Laplace
 * Copyright (C) 2022, 2023 Institut de Physique du Globe de Paris
 * Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2022, 2023 Observatoire de Paris
 * Copyright (C) 2022, 2023 Université de Reims Champagne-Ardenne
 * Copyright (C) 2022, 2023 Université de Versaille Saint-Quentin
 * Copyright (C) 2022, 2023 Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef RNATM_VOXEL_H
#define RNATM_VOXEL_H

#include "rnatm.h"

/*
 * Memory layout of a voxel
 * ------------------------
 *
 * The data of a voxel are stored in a list of 4 single-precision floating-point
 * numbers ka_min, ka_max, ks_min, ks_max.  For a given voxel, the data
 * corresponding to the operation 'O' on the coefficient 'C' are stored at the
 * index 'id' between [0, N-1] and calculated as follows:
 *
 *    id = C * RNATM_SVX_OPS_COUNT__ + O
 */

/* Total number of floating-point numbers per voxel */
#define NFLOATS_PER_VOXEL 4

/* Calculate the data index of voxel */
static FINLINE size_t
voxel_idata
  (const enum rnatm_radcoef radcoef,
   const enum rnatm_svx_op op)
{
  ASSERT(radcoef == RNATM_RADCOEF_Ka || radcoef == RNATM_RADCOEF_Ks);
  ASSERT((unsigned)op < RNATM_SVX_OPS_COUNT__);
  return radcoef*RNATM_SVX_OPS_COUNT__ + op;
}

static FINLINE void
voxel_clear(float* voxel)
{
  voxel[voxel_idata(RNATM_RADCOEF_Ka, RNATM_SVX_OP_MIN)] = FLT_MAX;
  voxel[voxel_idata(RNATM_RADCOEF_Ka, RNATM_SVX_OP_MAX)] =-FLT_MAX;
  voxel[voxel_idata(RNATM_RADCOEF_Ks, RNATM_SVX_OP_MIN)] = FLT_MAX;
  voxel[voxel_idata(RNATM_RADCOEF_Ks, RNATM_SVX_OP_MAX)] =-FLT_MAX;
}

static FINLINE void
voxel_accum(float* dst, const float* src)
{
  const size_t ka_min = voxel_idata(RNATM_RADCOEF_Ka, RNATM_SVX_OP_MIN);
  const size_t ka_max = voxel_idata(RNATM_RADCOEF_Ka, RNATM_SVX_OP_MAX);
  const size_t ks_min = voxel_idata(RNATM_RADCOEF_Ks, RNATM_SVX_OP_MIN);
  const size_t ks_max = voxel_idata(RNATM_RADCOEF_Ks, RNATM_SVX_OP_MAX);
  ASSERT(dst && src);

  if(src[ka_max] < src[ka_min]) return; /* Discard empty voxel */
  ASSERT(src[ks_max] >= src[ks_min]);

  dst[ka_max] += src[ka_max];
  dst[ks_max] += src[ks_max];
  dst[ka_min] = MMIN(dst[ka_min], src[ka_min]);
  dst[ks_min] = MMIN(dst[ks_min], src[ks_min]);
}

#endif /* RNATM_VOXEL_H */
