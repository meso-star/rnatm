/* Copyright (C) 2022, 2023 Centre National de la Recherche Scientifique
 * Copyright (C) 2022, 2023 Institut Pierre-Simon Laplace
 * Copyright (C) 2022, 2023 Institut de Physique du Globe de Paris
 * Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2022, 2023 Observatoire de Paris
 * Copyright (C) 2022, 2023 Université de Reims Champagne-Ardenne
 * Copyright (C) 2022, 2023 Université de Versaille Saint-Quentin
 * Copyright (C) 2022, 2023 Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "rnatm.h"
#include "rnatm_c.h"
#include "rnatm_log.h"

#include <rad-net/rnsf.h>
#include <rad-net/rnsl.h>

#include <star/sars.h>
#include <star/sbuf.h>
#include <star/sck.h>
#include <star/ssf.h>
#include <star/suvm.h>

#include <rsys/clock_time.h>
#include <rsys/cstr.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static res_T
check_rnatm_get_radcoef_args
  (const struct rnatm* atm,
   const struct rnatm_get_radcoef_args* args)
{
  size_t i, n;

  if(!args) return RES_BAD_ARG;

  /* Invalid cells */
  if(!args->cells) return RES_BAD_ARG;

  /* Invalid band index */
  n = darray_band_size_get(&atm->bands) - 1;
  if(args->iband < darray_band_cdata_get(&atm->bands)[0].index
  || args->iband > darray_band_cdata_get(&atm->bands)[n].index)
    return RES_BAD_ARG;

  /* Invalid quadrature point index */
  i = args->iband - darray_band_cdata_get(&atm->bands)[0].index;
  ASSERT(i <= n && args->iband == darray_band_cdata_get(&atm->bands)[i].index);
  if(args->iquad >= darray_band_cdata_get(&atm->bands)[i].nquad_pts)
    return RES_BAD_ARG;

  /* Invalid radiative coefficient */
  if((unsigned)args->radcoef >= RNATM_RADCOEFS_COUNT__)
    return RES_BAD_ARG;

  /* Invalid K range */
  if(args->k_min > args->k_max)
    return RES_BAD_ARG;

  return RES_OK;
}

static res_T
check_rnatm_sample_component_args
  (const struct rnatm* atm,
   const struct rnatm_sample_component_args* args)
{
  size_t i, n;

  if(!args) return RES_BAD_ARG;

  /* Invalid cells */
  if(!args->cells) return RES_BAD_ARG;

  /* Invalid band index */
  n = darray_band_size_get(&atm->bands) - 1;
  if(args->iband < darray_band_cdata_get(&atm->bands)[0].index
  || args->iband > darray_band_cdata_get(&atm->bands)[n].index)
    return RES_BAD_ARG;

  /* Invalid quadrature point index */
  i = args->iband - darray_band_cdata_get(&atm->bands)[0].index;
  ASSERT(i <= n && args->iband == darray_band_cdata_get(&atm->bands)[i].index);
  if(args->iquad >= darray_band_cdata_get(&atm->bands)[i].nquad_pts)
    return RES_BAD_ARG;

  /* Invalid radiative coefficient */
  if((unsigned)args->radcoef >= RNATM_RADCOEFS_COUNT__)
    return RES_BAD_ARG;

  /* Invalid random number */
  if(args->r < 0 || args->r >= 1)
    return RES_BAD_ARG;

  return RES_OK;
}

static INLINE res_T
check_cell(const struct rnatm_cell_pos* cell)
{
  double sum_bcoords;

  if(!cell) return RES_BAD_ARG;

  /* Invalid geometric primitive */
  if(SUVM_PRIMITIVE_NONE(&cell->prim)
  || cell->prim.nvertices != 4) /* Expect a tetraheron */
    return RES_BAD_ARG;

 /* Invalid barycentric coordinates */
  sum_bcoords =
    cell->barycentric_coords[0]
  + cell->barycentric_coords[1]
  + cell->barycentric_coords[2]
  + cell->barycentric_coords[3];
  if(!eq_eps(sum_bcoords, 1, 1.e-6))
    return RES_BAD_ARG;

  return RES_OK;
}

static res_T
check_rnatm_cell_get_radcoef_args
  (const struct rnatm* atm,
   const struct rnatm_cell_get_radcoef_args* args)
{
  size_t i, n;
  res_T res = RES_OK;
  ASSERT(atm && darray_band_size_get(&atm->bands));

  if(!args) return RES_BAD_ARG;

  /* Invalid cell */
  res = check_cell(&args->cell);
  if(res != RES_OK) return res;

  /* Invalid component */
  if(args->cell.component != RNATM_GAS
  && args->cell.component >= darray_aerosol_size_get(&atm->aerosols))
    return RES_BAD_ARG;

  /* Invalid band index */
  n = darray_band_size_get(&atm->bands) - 1;
  if(args->iband < darray_band_cdata_get(&atm->bands)[0].index
  || args->iband > darray_band_cdata_get(&atm->bands)[n].index)
    return RES_BAD_ARG;

  /* Invalid quadrature point index */
  i = args->iband - darray_band_cdata_get(&atm->bands)[0].index;
  ASSERT(i <= n && args->iband == darray_band_cdata_get(&atm->bands)[i].index);
  if(args->iquad >= darray_band_cdata_get(&atm->bands)[i].nquad_pts)
    return RES_BAD_ARG;

  /* Invalid radiative coefficient */
  if((unsigned)args->radcoef >= RNATM_RADCOEFS_COUNT__)
    return RES_BAD_ARG;

  /* Invalid K range */
  if(args->k_max < 0)
    return RES_BAD_ARG;

  return RES_OK;
}

static res_T
check_rnatm_cell_create_phase_fn_args
  (struct rnatm* atm,
   const struct rnatm_cell_create_phase_fn_args* args)
{
  res_T res = RES_OK;
  ASSERT(atm);

  if(!args) return RES_BAD_ARG;

  /* Invalid cell */
  res = check_cell(&args->cell);
  if(res != RES_OK) return res;

  /* Invalid component */
  if(args->cell.component != RNATM_GAS
  && args->cell.component >= darray_aerosol_size_get(&atm->aerosols))
    return RES_BAD_ARG;

  /* Invalid wavelength */
  if(args->wavelength < 0)
    return RES_BAD_ARG;

  /* Invalid random numbers */
  if(args->r[0] < 0 || args->r[0] >= 1
  || args->r[1] < 0 || args->r[1] >= 1)
    return RES_BAD_ARG;

  return RES_OK;
}

static INLINE void
reset_gas_properties(struct gas* gas)
{
  if(gas->temperatures) SBUF(ref_put(gas->temperatures));
  if(gas->ck) SCK(ref_put(gas->ck));
  gas->temperatures = NULL;
  gas->ck = NULL;
}

static INLINE void
reset_aerosol_properties(struct aerosol* aerosol)
{
  if(aerosol->phase_fn_ids) SBUF(ref_put(aerosol->phase_fn_ids));
  if(aerosol->sars) SARS(ref_put(aerosol->sars));
  aerosol->phase_fn_ids = NULL;
  aerosol->sars = NULL;
  darray_phase_fn_clear(&aerosol->phase_fn_lst);
}

static INLINE res_T
check_gas_temperatures_desc
  (const struct rnatm* atm,
   const struct sbuf_desc* desc,
   const struct rnatm_gas_args* gas_args)
{
  ASSERT(atm && desc && gas_args);

  if(desc->size != atm->gas.nvertices) {
    log_err(atm,
      "%s: no sufficient temperatures regarding the mesh %s\n",
      gas_args->temperatures_filename, gas_args->smsh_filename);
    return RES_BAD_ARG;
  }

  if(desc->szitem != 4 || desc->alitem != 4 || desc->pitch != 4) {
    log_err(atm, "%s: unexpected layout of temperatures\n",
      gas_args->temperatures_filename);
    return RES_BAD_ARG;
  }

  return RES_OK;
}

static res_T
check_gas_temperatures(const struct rnatm* atm)
{
  struct sbuf_desc sbuf_desc = SBUF_DESC_NULL;
  size_t i;
  res_T res = RES_OK;
  ASSERT(atm);

  /* The layout of sbuf_desc has already been checked when creating rnatm */
  res = sbuf_get_desc(atm->gas.temperatures, &sbuf_desc);
  if(res != RES_OK) goto error;

  FOR_EACH(i, 0, sbuf_desc.size) {
    const float temperature = *((const float*)sbuf_desc_at(&sbuf_desc, i));
    if(temperature != temperature /* NaN */ || temperature < 0) {
      log_err(atm, "%s: node %lu: invalid gas temperature `%g'\n",
        str_cget(&atm->name), i, temperature);
      res = RES_BAD_ARG;
      goto error;
    }
  }

exit:
  return res;
error:
  goto exit;
}

static INLINE res_T
check_gas_ck_desc
  (const struct rnatm* atm,
   const struct rnatm_gas_args* gas_args)
{
  ASSERT(atm && gas_args);

  if(sck_get_nodes_count(atm->gas.ck) != atm->gas.nvertices) {
    log_err(atm,
      "%s: no sufficient correlated-K regarding the mesh %s\n",
      gas_args->sck_filename, gas_args->smsh_filename);
    return RES_BAD_ARG;
  }
  return RES_OK;
}

static INLINE res_T
check_aerosol_phase_fn_ids_desc
  (const struct rnatm* atm,
   const struct aerosol* aerosol,
   const struct sbuf_desc* desc,
   const struct rnatm_aerosol_args* aerosol_args)
{
  ASSERT(atm && aerosol && desc && aerosol_args);

  if(desc->size != aerosol->nvertices) {
    log_err(atm,
      "%s: no sufficient phase function ids regarding the mesh %s\n",
      aerosol_args->phase_fn_ids_filename, aerosol_args->smsh_filename);
    return RES_BAD_ARG;
  }

  if(desc->szitem != 4 || desc->alitem != 4 || desc->pitch != 4) {
    log_err(atm, "%s: unexpected layout of phase function ids\n",
      aerosol_args->phase_fn_ids_filename);
    return RES_BAD_ARG;
  }

  return RES_OK;
}

static res_T
check_aerosol_phase_fn_ids
  (const struct rnatm* atm,
   const struct aerosol* aerosol)
{
  struct sbuf_desc sbuf_desc = SBUF_DESC_NULL;
  size_t i;
  res_T res = RES_OK;
  ASSERT(atm && aerosol);

  /* The layout of sbuf_desc has already been checked when creating rnatm */
  res = sbuf_get_desc(aerosol->phase_fn_ids, &sbuf_desc);
  if(res != RES_OK) goto error;

  FOR_EACH(i, 0, sbuf_desc.size) {
    const uint32_t id = *((uint32_t*)sbuf_desc_at(&sbuf_desc, i));

    if(id >= darray_phase_fn_size_get(&aerosol->phase_fn_lst)) {
      log_err(atm,
        "%s: node %lu: invalid phase function id `%lu'. It must be in [0, %lu[\n",
        str_cget(&atm->name), (unsigned long)i, (unsigned long)id,
        (unsigned long)darray_phase_fn_size_get(&aerosol->phase_fn_lst));
      res = RES_BAD_ARG;
      goto error;
    }
  }

exit:
  return res;
error:
  goto exit;
}

static INLINE res_T
check_aerosol_sars_desc
  (const struct rnatm* atm,
   const struct aerosol* aerosol,
   const struct rnatm_aerosol_args* aerosol_args)
{
  ASSERT(atm && aerosol && aerosol_args);

  if(sars_get_nodes_count(aerosol->sars) != aerosol->nvertices) {
    log_err(atm,
      "%s: no sufficient radiative properties regarding the mesh %s\n",
      aerosol_args->sars_filename, aerosol_args->smsh_filename);
    return RES_BAD_ARG;
  }
  return RES_OK;
}

static INLINE res_T
create_phase_fn_rayleigh(struct rnatm* atm, struct ssf_phase** phase_fn)
{
  res_T res = RES_OK;
  ASSERT(atm && phase_fn);

  res = ssf_phase_create(atm->allocator, &ssf_phase_rayleigh, phase_fn);
  if(res != RES_OK) goto error;

exit:
  return res;
error:
  if(*phase_fn) { SSF(phase_ref_put(*phase_fn)); *phase_fn = NULL; }
  goto exit;
}

static INLINE res_T
create_phase_fn_hg
  (struct rnatm* atm,
   const struct rnsf_phase_fn_hg* hg,
   struct ssf_phase** out_ssf_phase)
{
  struct ssf_phase* ssf_phase = NULL;
  res_T res = RES_OK;
  ASSERT(atm && hg && out_ssf_phase);

  res = ssf_phase_create(atm->allocator, &ssf_phase_hg, &ssf_phase);
  if(res != RES_OK) goto error;
  res = ssf_phase_hg_setup(ssf_phase, hg->g);
  if(res != RES_OK) goto error;

exit:
  *out_ssf_phase = ssf_phase;
  return res;
error:
  if(ssf_phase) { SSF(phase_ref_put(ssf_phase)); ssf_phase = NULL; }
  goto exit;
}

static void
phase_fn_discrete_get_item
  (const size_t iitem,
   struct ssf_discrete_item* item,
   void* context)
{
  const struct rnsf_phase_fn_discrete* discrete = context;
  ASSERT(item && context && iitem < discrete->nitems);
  item->theta = discrete->items[iitem].theta;
  item->value = discrete->items[iitem].value;
}

static INLINE res_T
create_phase_fn_discrete
  (struct rnatm* atm,
   struct rnsf_phase_fn_discrete* discrete,
   struct ssf_phase** out_ssf_phase)
{
  struct ssf_discrete_setup_args args = SSF_DISCRETE_SETUP_ARGS_NULL;
  struct ssf_phase* ssf_phase = NULL;
  res_T res = RES_OK;
  ASSERT(atm && discrete && out_ssf_phase);

  res = ssf_phase_create(atm->allocator, &ssf_phase_discrete, &ssf_phase);
  if(res != RES_OK) goto error;

  args.get_item = phase_fn_discrete_get_item;
  args.context = discrete;
  args.nitems = discrete->nitems;
  res = ssf_phase_discrete_setup(ssf_phase, &args);
  if(res != RES_OK) goto error;

exit:
  *out_ssf_phase = ssf_phase;
  return res;
error:
  if(ssf_phase) { SSF(phase_ref_put(ssf_phase)); ssf_phase = NULL; }
  goto exit;
}

static res_T
create_phase_fn
  (struct rnatm* atm,
   const struct rnsf_phase_fn* phase_fn,
   struct ssf_phase** out_ssf_phase)
{
  struct rnsf_phase_fn_hg hg;
  struct rnsf_phase_fn_discrete discrete;
  struct ssf_phase* ssf_phase = NULL;
  res_T res = RES_OK;
  ASSERT(atm && phase_fn && out_ssf_phase);

  /* Create the scattering function */
  switch(rnsf_phase_fn_get_type(phase_fn)) {
    case RNSF_PHASE_FN_HG:
      RNSF(phase_fn_get_hg(phase_fn, &hg));
      res = create_phase_fn_hg(atm, &hg, &ssf_phase);
      if(res != RES_OK) goto error;
      break;
    case RNSF_PHASE_FN_DISCRETE:
      RNSF(phase_fn_get_discrete(phase_fn, &discrete));
      res = create_phase_fn_discrete(atm, &discrete, &ssf_phase);
      if(res != RES_OK) goto error;
      break;
    default: FATAL("Unreachable code\n");  break;
  }

exit:
  *out_ssf_phase = ssf_phase;
  return res;
error:
  if(ssf_phase) { SSF(phase_ref_put(ssf_phase)); ssf_phase = NULL; }
  goto exit;
}

static res_T
phase_fn_create_phase_list
  (struct rnatm* atm,
   const char* filename,
   struct phase_fn* phase_fn)
{
  size_t nphases = 0;
  size_t iphase = 0;
  res_T res = RES_OK;
  ASSERT(atm && filename && phase_fn);

  /* Reserve memory space for the list of phase functions */
  nphases = rnsf_get_phase_fn_count(phase_fn->rnsf);
  res = darray_phase_resize(&phase_fn->phase_lst, nphases);
  if(res != RES_OK) goto error;

  FOR_EACH(iphase, 0, nphases) {
    const struct rnsf_phase_fn* fn = rnsf_get_phase_fn(phase_fn->rnsf, iphase);
    struct ssf_phase** phase = darray_phase_data_get(&phase_fn->phase_lst) + iphase;

    res = create_phase_fn(atm, fn, phase);
    if(res != RES_OK) goto error;
  }

exit:
  return res;

error:
  log_err(atm, "%s: error creating the lisf of phase functions -- %s\n",
    filename, res_to_cstr(res));
  darray_phase_clear(&phase_fn->phase_lst);
  goto exit;
}

static res_T
load_phase_fn
  (struct rnatm* atm,
   const char* filename,
   struct rnsf** out_phase_fn)
{
  struct rnsf_create_args args = RNSF_CREATE_ARGS_DEFAULT;
  struct rnsf* phase_fn = NULL;
  res_T res = RES_OK;
  ASSERT(atm && filename && out_phase_fn);

  args.verbose = atm->verbose;
  args.logger = atm->logger;
  args.allocator = atm->allocator;
  res = rnsf_create(&args, &phase_fn);
  if(res != RES_OK) {
    log_err(atm,
      "%s: could not create the Rad-Net Scattering Function data structure\n",
      filename);
    goto error;
  }

  res = rnsf_load(phase_fn, filename);
  if(res != RES_OK) goto error;

exit:
  *out_phase_fn = phase_fn;
  return res;
error:
  if(phase_fn) { RNSF(ref_put(phase_fn)); phase_fn = NULL; }
  goto exit;
}

static res_T
load_phase_fn_list
  (struct rnatm* atm,
   struct aerosol* aerosol,
   const struct rnatm_aerosol_args* args)
{
  struct rnsl_create_args rnsl_args = RNSL_CREATE_ARGS_DEFAULT;
  struct rnsl* rnsl = NULL;
  size_t iphase_fn, nphase_fn;
  res_T res = RES_OK;

  /* Create loader of scattefing function paths */
  rnsl_args.logger = atm->logger;
  rnsl_args.allocator = atm->allocator;
  rnsl_args.verbose = atm->verbose;
  res = rnsl_create(&rnsl_args, &rnsl);
  if(res != RES_OK) {
    log_err(atm,
      "Failed to create loader for phase function list `%s' -- %s\n",
      args->phase_fn_lst_filename, res_to_cstr(res));
    goto error;
  }

  /* Load the list of phase function paths */
  res = rnsl_load(rnsl, args->phase_fn_lst_filename);
  if(res != RES_OK) goto error;

  /* Reserve memory space for the list of phase functions */
  nphase_fn = rnsl_get_strings_count(rnsl);
  res = darray_phase_fn_resize(&aerosol->phase_fn_lst, nphase_fn);
  if(res != RES_OK) {
    log_err(atm, "%s: could not allocate the list of %lu phase functions -- %s\n",
      args->phase_fn_lst_filename, nphase_fn, res_to_cstr(res));
    goto error;
  }

  FOR_EACH(iphase_fn, 0, nphase_fn) {
    const char* filename = rnsl_get_string(rnsl, iphase_fn);
    struct phase_fn* phase_fn =
      darray_phase_fn_data_get(&aerosol->phase_fn_lst)+iphase_fn;

    res = load_phase_fn(atm, filename, &phase_fn->rnsf);
    if(res != RES_OK) goto error;
    res = phase_fn_create_phase_list(atm, filename, phase_fn);
    if(res != RES_OK) goto error;
  }

exit:
  if(rnsl) RNSL(ref_put(rnsl));
  return res;
error:
  darray_phase_fn_clear(&aerosol->phase_fn_lst);
  goto exit;
}

static res_T
setup_gas_properties(struct rnatm* atm, const struct rnatm_gas_args* gas_args)
{
  char buf[128];
  struct time t0, t1;
  struct sck_load_args sck_load_args = SCK_LOAD_ARGS_NULL;
  struct sck_band band_low = SCK_BAND_NULL;
  struct sck_band band_upp = SCK_BAND_NULL;
  struct sck_create_args sck_args = SCK_CREATE_ARGS_DEFAULT;
  struct sbuf_create_args sbuf_args = SBUF_CREATE_ARGS_DEFAULT;
  struct sbuf_desc sbuf_desc = SBUF_DESC_NULL;
  size_t nbands;

  res_T res = RES_OK;
  ASSERT(atm && gas_args);

  /* Start time recording */
  log_info(atm, "load gas properties\n");
  time_current(&t0);

  /* Create the Rayleigh phase function */
  res = ssf_phase_create(atm->allocator, &ssf_phase_rayleigh, &atm->gas.rayleigh);
  if(res != RES_OK) goto error;

  /* Create the Star-Buffer loader */
  sbuf_args.logger = atm->logger;
  sbuf_args.allocator = atm->allocator;
  sbuf_args.verbose = atm->verbose;
  res = sbuf_create(&sbuf_args, &atm->gas.temperatures);
  if(res != RES_OK) goto error;

  /* Load gas temperatures */
  res = sbuf_load(atm->gas.temperatures, gas_args->temperatures_filename);
  if(res != RES_OK) goto error;
  res = sbuf_get_desc(atm->gas.temperatures, &sbuf_desc);
  if(res != RES_OK) goto error;
  res = check_gas_temperatures_desc(atm, &sbuf_desc, gas_args);
  if(res != RES_OK) goto error;

  /* Create the Star-CK loader */
  sck_args.logger = atm->logger;
  sck_args.allocator = atm->allocator;
  sck_args.verbose = atm->verbose;
  res = sck_create(&sck_args, &atm->gas.ck);
  if(res != RES_OK) goto error;

  /* Load correlated-K */
  sck_load_args.path = gas_args->sck_filename;
  sck_load_args.memory_mapping = 1;
  res = sck_load(atm->gas.ck, &sck_load_args);
  if(res != RES_OK) goto error;
  res = check_gas_ck_desc(atm, gas_args);
  if(res != RES_OK) goto error;

  /* Print elapsed time */
  time_sub(&t0, time_current(&t1), &t0);
  time_dump(&t0, TIME_ALL, NULL, buf, sizeof(buf));
  log_info(atm, "gas properties loaded in %s\n", buf);

  /* Print gas informations */
  nbands = sck_get_bands_count(atm->gas.ck);
  res = sck_get_band(atm->gas.ck, 0, &band_low);
  if(res != RES_OK) goto error;
  res = sck_get_band(atm->gas.ck, nbands-1, &band_upp);
  if(res != RES_OK) goto error;
  log_info(atm, "the gas is composed of %lu band%sin [%g, %g[ nm\n",
    nbands, nbands > 1 ? "s " : " ",
    band_low.lower,
    band_upp.upper);

exit:
  return res;
error:
  reset_gas_properties(&atm->gas);
  goto exit;
}

static res_T
setup_aerosol_properties
  (struct rnatm* atm,
   struct aerosol* aerosol,
   const struct rnatm_aerosol_args* aerosol_args)
{
  char buf[128];
  size_t ibands[2];
  struct time t0, t1;
  struct sars_create_args sars_args = SARS_CREATE_ARGS_DEFAULT;
  struct sars_load_args sars_load_args = SARS_LOAD_ARGS_NULL;
  struct sbuf_create_args sbuf_args = SBUF_CREATE_ARGS_DEFAULT;
  struct sbuf_desc sbuf_desc = SBUF_DESC_NULL;

  res_T res = RES_OK;
  ASSERT(atm && aerosol_args);

  /* Start time recording */
  log_info(atm, "load %s properties\n", str_cget(&aerosol->name));
  time_current(&t0);

  /* Create the Star-Aerosol loader */
  sars_args.logger = atm->logger;
  sars_args.allocator = atm->allocator;
  sars_args.verbose = atm->verbose;
  res = sars_create(&sars_args, &aerosol->sars);
  if(res != RES_OK) goto error;

  /* Load the aerosol radiative properties */
  sars_load_args.path = aerosol_args->sars_filename;
  sars_load_args.memory_mapping = 1;
  res = sars_load(aerosol->sars, &sars_load_args);
  if(res != RES_OK) goto error;
  res = check_aerosol_sars_desc(atm, aerosol, aerosol_args);
  if(res != RES_OK) goto error;

  /* Ignore the aerosol if it has no data for the input spectral range */
  res = sars_find_bands(aerosol->sars, atm->spectral_range, ibands);
  if(res != RES_OK) goto error;
  if(ibands[0] > ibands[1]) {
    reset_aerosol_properties(aerosol);
    goto exit;
  }

  /* Load the aerosol phase functions */
  res = load_phase_fn_list(atm, aerosol, aerosol_args);
  if(res != RES_OK) goto error;

  /* Create the Star-Buffer loader */
  sbuf_args.logger = atm->logger;
  sbuf_args.allocator = atm->allocator;
  sbuf_args.verbose = atm->verbose;
  res = sbuf_create(&sbuf_args, &aerosol->phase_fn_ids);
  if(res != RES_OK) goto error;

  /* Load phase function ids */
  res = sbuf_load(aerosol->phase_fn_ids, aerosol_args->phase_fn_ids_filename);
  if(res != RES_OK) goto error;
  res = sbuf_get_desc(aerosol->phase_fn_ids, &sbuf_desc);
  if(res != RES_OK) goto error;
  res = check_aerosol_phase_fn_ids_desc(atm, aerosol, &sbuf_desc, aerosol_args);
  if(res != RES_OK) goto error;

  /* Print elapsed time */
  time_sub(&t0, time_current(&t1), &t0);
  time_dump(&t0, TIME_ALL, NULL, buf, sizeof(buf));
  log_info(atm, "%s properties loaded in %s\n",
    str_cget(&aerosol->name), buf);

exit:
  return res;
error:
  reset_aerosol_properties(aerosol);
  goto exit;
}

static res_T
remove_useless_aerosols(struct rnatm* atm)
{
  struct aerosol* aerosols = NULL;
  size_t i, n;
  res_T res = RES_OK;
  ASSERT(atm);

  aerosols = darray_aerosol_data_get(&atm->aerosols);

  n = 0;
  FOR_EACH(i, 0, darray_aerosol_size_get(&atm->aerosols)) {
    /* Discard aerosols with no radiative properties */
    if(!aerosols[i].sars) {
      log_warn(atm,
        "discard %s aerosol: no radiative properties are defined for the "
        "spectral domain [%g, %g] nm\n",
        str_cget(&aerosols[i].name),
        atm->spectral_range[0],
        atm->spectral_range[1]);
      continue;
    }
    /* Compact the aerosols to consider */
    if(i != n) {
      aerosol_copy_and_release(aerosols+n, aerosols+i);
    }
    ++n;
  }

  if(!n) {
    /* Discard all aerosols */
    darray_aerosol_purge(&atm->aerosols);
  } else {
    /* Resize the aerosols list */
    res = darray_aerosol_resize(&atm->aerosols, n);
    if(res != RES_OK) goto error;
  }

exit:
  return res;
error:
  goto exit;
}

static res_T
find_band_range
  (const struct rnatm* atm,
   const double range[2], /* In nanometers */
   size_t bands[2])
{
  struct sck_band band_low;
  struct sck_band band_upp;
  size_t nbands_overlaped;
  size_t iband;
  res_T res = RES_OK;
  ASSERT(atm && range && bands && range[0] <= range[1]);

  res = sck_find_bands(atm->gas.ck, range, bands);
  if(res != RES_OK) goto error;

  if(bands[0] > bands[1]) {
    log_err(atm,
      "the spectral range [%g, %g] nm does not overlap any bands\n",
      SPLIT2(range));
    res = RES_BAD_ARG;
    goto error;
  }

  /* Check that there is no hole in the spectral data */
  FOR_EACH(iband, bands[0], bands[1]) {
    struct sck_band band_curr;
    struct sck_band band_next;

    SCK(get_band(atm->gas.ck, iband+0, &band_curr));
    SCK(get_band(atm->gas.ck, iband+1, &band_next));

    if(band_curr.upper != band_next.lower) {
      log_err(atm,
        "gas spectral data is missing in [%g, %g] nm. "
        "There is a hole in [%g, %g] nm\n",
        atm->spectral_range[0],
        atm->spectral_range[1],
        band_curr.upper,
        band_next.lower);
      res = RES_BAD_ARG;
      goto error;
    }
  }


  SCK(get_band(atm->gas.ck, bands[0], &band_low));
  SCK(get_band(atm->gas.ck, bands[1], &band_upp));
  if(band_low.lower > range[0]
  || band_upp.upper < range[1]) {
    log_err(atm,
      "gas spectral data is missing. They are defined between [%g, %g] nm "
      "while the required spectral range is [%g, %g]\n",
      band_low.lower,
      band_upp.upper,
      range[0],
      range[1]);
    res = RES_BAD_ARG;
    goto error;
  }

  nbands_overlaped = bands[1] - bands[0] + 1;

  log_info(atm,
    "the spectral range [%g, %g] nm overlaps %lu band%sin [%g, %g[ nm\n",
    SPLIT2(range),
    (unsigned long)nbands_overlaped,
    nbands_overlaped > 1 ? "s ": " ",
    band_low.lower,
    band_upp.upper);

exit:
  return res;
error:
  goto exit;
}

static res_T
setup_spectral_range(struct rnatm* atm, const struct rnatm_create_args* args)
{
  size_t iband, nbands;
  size_t offset;
  size_t i;
  res_T res = RES_OK;
  ASSERT(atm && args);
  ASSERT(args->spectral_range[0] <= args->spectral_range[1]);

  atm->spectral_range[0] = args->spectral_range[0];
  atm->spectral_range[1] = args->spectral_range[1];

  /* Find the bands overlapped by the input spectral range */
  res = find_band_range(atm, atm->spectral_range, atm->ibands);
  if(res != RES_OK) goto error;

  /* Allocate the list of bands to be taken into account */
  nbands = atm->ibands[1] - atm->ibands[0] + 1;
  res = darray_band_resize(&atm->bands, nbands);
  if(res != RES_OK) {
    log_err(atm,
      "failed to register the bands to be taken into account -- %s\n",
      res_to_cstr(res));
    goto error;
  }

  /* Register the bands */
  i = 0;
  offset = 0;
  FOR_EACH(iband, atm->ibands[0], atm->ibands[1]+1) {
    struct band* band = NULL;
    struct sck_band sck_band;

    SCK(get_band(atm->gas.ck, iband, &sck_band));

    band = darray_band_data_get(&atm->bands) + i;
    band->index = iband;
    band->nquad_pts = sck_band.quad_pts_count;
    band->offset_accel_struct = offset;

    offset += band->nquad_pts;
    ++i;
  }

exit:
  return res;
error:
  goto exit;
}

static res_T
cell_create_phase_fn_aerosol
  (struct rnatm* atm,
   const struct rnatm_cell_create_phase_fn_args* args,
   struct ssf_phase** out_ssf_phase)
{
  struct sbuf_desc sbuf_desc;
  const double* bcoords = NULL;
  struct aerosol* aerosol = NULL;
  struct phase_fn* phase_fn = NULL;
  struct ssf_phase* ssf_phase = NULL;
  uint32_t phase_fn_id = 0;
  size_t inode = 0;
  size_t iphase_fn = 0;
  int icell_node = 0;
  res_T res = RES_OK;
  ASSERT(atm && args && out_ssf_phase);
  ASSERT(args->cell.component < darray_aerosol_size_get(&atm->aerosols));

  /* Get the aerosol the cell belongs to */
  aerosol = darray_aerosol_data_get(&atm->aerosols) + args->cell.component;

  /* Sample the cell node to consider */
  bcoords = args->cell.barycentric_coords;
  if(args->r[0] < bcoords[0]) {
    icell_node = 0;
  } else if(args->r[0] < bcoords[0] + bcoords[1]) {
    icell_node = 1;
  } else if(args->r[0] < bcoords[0] + bcoords[1] + bcoords[2]) {
    icell_node = 2;
  } else {
    icell_node = 3;
  }

  /* Retrieve the phase function id of the node */
  SBUF(get_desc(aerosol->phase_fn_ids, &sbuf_desc));
  inode = args->cell.prim.indices[icell_node];
  ASSERT(inode < sbuf_desc.size);
  phase_fn_id = *((const uint32_t*)sbuf_desc_at(&sbuf_desc, inode));

  /* Retrieve the spectrally varying phase function of the node */
  ASSERT(phase_fn_id < darray_phase_fn_size_get(&aerosol->phase_fn_lst));
  phase_fn = darray_phase_fn_data_get(&aerosol->phase_fn_lst)+phase_fn_id;

  /* Get the phase function based on the input wavelength */
  res = rnsf_fetch_phase_fn
    (phase_fn->rnsf, args->wavelength, args->r[1], &iphase_fn);
  if(res != RES_OK) goto error;
  ssf_phase = darray_phase_data_get(&phase_fn->phase_lst)[iphase_fn];
  ASSERT(ssf_phase);
  SSF(phase_ref_get(ssf_phase));

exit:
  if(out_ssf_phase) *out_ssf_phase = ssf_phase;
  return res;
error:
  log_err(atm, "error creating the %s phase function -- %s\n",
    str_cget(&aerosol->name), res_to_cstr(res));
  if(ssf_phase) { SSF(phase_ref_put(ssf_phase)); ssf_phase = NULL; }
  goto exit;
}

static res_T
compute_unnormalized_cumulative_radcoef
  (const struct rnatm* atm,
   const enum rnatm_radcoef radcoef,
   const struct rnatm_cell_pos* cells,
   const size_t iband,
   const size_t iquad,
   float cumulative[RNATM_MAX_COMPONENTS_COUNT],
   /* For debug */
   const double k_min,
   const double k_max)
{
  struct rnatm_cell_get_radcoef_args cell_args = RNATM_CELL_GET_RADCOEF_ARGS_NULL;
  size_t cpnt = RNATM_GAS;
  size_t icumul = 0;
  size_t naerosols = 0;
  float k = 0;
  res_T res = RES_OK;
  ASSERT(atm && cells && (unsigned)radcoef < RNATM_RADCOEFS_COUNT__);
  ASSERT(cumulative);
  (void)k_min;

  naerosols = darray_aerosol_size_get(&atm->aerosols);
  ASSERT(naerosols+1 < RNATM_MAX_COMPONENTS_COUNT);

  /* Setup the arguments common to all components */
  cell_args.iband = iband;
  cell_args.iquad = iquad;
  cell_args.radcoef = radcoef;
  cell_args.k_max = k_max; /* For Debug */

  do {

    cell_args.cell = cells[cpnt+1];

    /* Test if the component exists here */
    if(!SUVM_PRIMITIVE_NONE(&cell_args.cell.prim)) {
      double per_cell_k;

      /* Add the component's contribution to the radiative coefficient */
      res = rnatm_cell_get_radcoef(atm, &cell_args, &per_cell_k);
      if(res != RES_OK) goto error;
      k += (float)per_cell_k;
    }

    /* Update the cumulative */
    cumulative[icumul] = k;
    ++icumul;
  } while(++cpnt < naerosols);

  ASSERT(cumulative[icumul-1] == 0 || (float)k_min <= cumulative[icumul-1]);
  ASSERT(cumulative[icumul-1] == 0 || (float)k_max >= cumulative[icumul-1]);

exit:
  return res;
error:
  goto exit;
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
rnatm_get_radcoef
  (const struct rnatm* atm,
   const struct rnatm_get_radcoef_args* args,
   double* out_k)
{
  float cumul[RNATM_MAX_COMPONENTS_COUNT];
  size_t ncpnts;
  double k = 0;
  res_T res = RES_OK;

  if(!atm || !out_k) { res = RES_BAD_ARG; goto error; }
  res = check_rnatm_get_radcoef_args(atm, args);
  if(res != RES_OK) goto error;

  ncpnts = 1/*gas*/ + darray_aerosol_size_get(&atm->aerosols);
  ASSERT(ncpnts <= RNATM_MAX_COMPONENTS_COUNT);

  /* Calculate the cumulative (unnormalized) of radiative coefficients. Its last
   * entry is the sum of the radiative coefficients of each component, which is
   * the atmospheric radiative coefficient to be returned. */
  res = compute_unnormalized_cumulative_radcoef(atm, args->radcoef,
    args->cells, args->iband, args->iquad, cumul, args->k_min, args->k_max);
  if(res != RES_OK) goto error;

  if(cumul[ncpnts-1] == 0) {
    k = 0; /* No atmospheric data */
  } else {
    k = cumul[ncpnts-1];
    ASSERT(args->k_min <= k && k <= args->k_max);
  }

exit:
  *out_k = k;
  return res;
error:
  goto exit;
}

res_T
rnatm_sample_component
  (const struct rnatm* atm,
   const struct rnatm_sample_component_args* args,
   size_t* cpnt)
{
  float cumul[RNATM_MAX_COMPONENTS_COUNT];
  float norm;
  size_t ncpnts;
  size_t i;
  res_T res = RES_OK;

  if(!atm || !cpnt) { res = RES_BAD_ARG; goto error; }
  res = check_rnatm_sample_component_args(atm, args);
  if(res != RES_OK) goto error;

  ncpnts = 1/*gas*/ + darray_aerosol_size_get(&atm->aerosols);
  ASSERT(ncpnts <= RNATM_MAX_COMPONENTS_COUNT);

  /* Discard the calculation of the cumulative if there is only gas */
  if(ncpnts == 1) {
    ASSERT(!SUVM_PRIMITIVE_NONE(&args->cells[0].prim));
    *cpnt = RNATM_GAS;
    goto exit;
  }

  res = compute_unnormalized_cumulative_radcoef(atm, args->radcoef, args->cells,
    args->iband, args->iquad, cumul, -DBL_MAX, DBL_MAX);
  if(res != RES_OK) goto error;
  ASSERT(cumul[ncpnts-1] > 0);

  /* Normalize the cumulative */
  norm = cumul[ncpnts-1];
  FOR_EACH(i, 0, ncpnts) cumul[i] /= norm;
  cumul[ncpnts-1] = 1.f; /* Handle precision issues */

  /* Use a simple linear search to sample the component since there are
   * usually very few aerosols to consider */
  FOR_EACH(i, 0, ncpnts) if(args->r < cumul[i]) break;

  *cpnt = i-1;
  ASSERT(*cpnt == RNATM_GAS || *cpnt < darray_aerosol_size_get(&atm->aerosols));
  ASSERT(!SUVM_PRIMITIVE_NONE(&args->cells[i].prim));

exit:
  return res;
error:
  goto exit;
}

res_T
rnatm_cell_get_radcoef
  (const struct rnatm* atm,
   const struct rnatm_cell_get_radcoef_args* args,
   double* out_k)
{
  float vtx_k[4];
  double k = NaN;
  res_T res = RES_OK;

  if(!atm || !out_k) { res = RES_BAD_ARG; goto error; }
  res = check_rnatm_cell_get_radcoef_args(atm, args);
  if(res != RES_OK) goto error;

  /* Get the radiative coefficients of the tetrahedron */
  tetra_get_radcoef(atm, &args->cell.prim, args->cell.component, args->iband,
    args->iquad, args->radcoef, vtx_k);

  if(vtx_k[0] == vtx_k[1]
  && vtx_k[0] == vtx_k[2]
  && vtx_k[0] == vtx_k[3]) {
    /* Avoid precision issue when iterpolating the same value */
    k = vtx_k[0];
  } else {
    float min_vtx_k;
    float max_vtx_k;

    /* Calculate the radiative coefficient by linearly interpolating the
     * coefficients defined by tetrahedron vertex */
    k = vtx_k[0] * args->cell.barycentric_coords[0]
      + vtx_k[1] * args->cell.barycentric_coords[1]
      + vtx_k[2] * args->cell.barycentric_coords[2]
      + vtx_k[3] * args->cell.barycentric_coords[3];

    /* Fix interpolation accuracy issues */
    min_vtx_k = MMIN(MMIN(vtx_k[0], vtx_k[1]), MMIN(vtx_k[2], vtx_k[3]));
    max_vtx_k = MMAX(MMAX(vtx_k[0], vtx_k[1]), MMAX(vtx_k[2], vtx_k[3]));
    k = CLAMP((float)k, min_vtx_k, max_vtx_k);
  }
  ASSERT(k <= args->k_max);

exit:
  *out_k = k;
  return res;
error:
  goto exit;
}

res_T
rnatm_cell_create_phase_fn
  (struct rnatm* atm,
   const struct rnatm_cell_create_phase_fn_args* args,
   struct ssf_phase** out_ssf_phase)
{
  struct ssf_phase* ssf_phase = NULL;
  res_T res = RES_OK;

  if(!atm || !out_ssf_phase) { res = RES_BAD_ARG; goto error; }
  res = check_rnatm_cell_create_phase_fn_args(atm, args);
  if(res != RES_OK) goto error;

  if(args->cell.component == RNATM_GAS) {
    /* Get a reference on the pre-allocated Rayleigh phase function */
    ssf_phase = atm->gas.rayleigh;
    SSF(phase_ref_get(ssf_phase));
  } else {
    res = cell_create_phase_fn_aerosol(atm, args, &ssf_phase);
    if(res != RES_OK) goto error;
  }

exit:
  if(out_ssf_phase) *out_ssf_phase = ssf_phase;
  return res;
error:
  if(ssf_phase) { SSF(phase_ref_put(ssf_phase)); ssf_phase = NULL; }
  goto exit;
}

res_T
rnatm_cell_get_gas_temperature
  (const struct rnatm* atm,
   const struct rnatm_cell_pos* cell,
   double* temperature)
{
  struct sbuf_desc sbuf_desc;
  float vtx_T[4];
  res_T res = RES_OK;

  if(!atm || !temperature) return RES_OK;
  res = check_cell(cell);
  if(res != RES_OK) goto error;

  /* Invalid cell regarding gas */
  SBUF(get_desc(atm->gas.temperatures, &sbuf_desc));
  if(cell->prim.indices[0] >= sbuf_desc.size
  || cell->prim.indices[1] >= sbuf_desc.size
  || cell->prim.indices[2] >= sbuf_desc.size
  || cell->prim.indices[3] >= sbuf_desc.size)
    return RES_BAD_ARG;

  /* Get the temperature per vertex */
  vtx_T[0] = *((float*)sbuf_desc_at(&sbuf_desc, cell->prim.indices[0]));
  vtx_T[1] = *((float*)sbuf_desc_at(&sbuf_desc, cell->prim.indices[1]));
  vtx_T[2] = *((float*)sbuf_desc_at(&sbuf_desc, cell->prim.indices[2]));
  vtx_T[3] = *((float*)sbuf_desc_at(&sbuf_desc, cell->prim.indices[3]));


  /* Calculate the temperature at the input position by linearly interpolating
   * the temperatures defined by tetrahedron vertex */
  *temperature =
    vtx_T[0] * cell->barycentric_coords[0]
  + vtx_T[1] * cell->barycentric_coords[1]
  + vtx_T[2] * cell->barycentric_coords[2]
  + vtx_T[3] * cell->barycentric_coords[3];

exit:
  return res;
error:
  goto exit;
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
res_T
setup_properties(struct rnatm* atm, const struct rnatm_create_args* args)
{
  size_t i = 0;
  res_T res = RES_OK;
  ASSERT(atm && args);

  res = setup_gas_properties(atm, &args->gas);
  if(res != RES_OK) goto error;

  res = setup_spectral_range(atm, args);
  if(res != RES_OK) goto error;

  FOR_EACH(i, 0, darray_aerosol_size_get(&atm->aerosols)) {
    struct aerosol* aerosol = darray_aerosol_data_get(&atm->aerosols)+i;
    res = setup_aerosol_properties(atm, aerosol, args->aerosols+i);
    if(res != RES_OK) goto error;
  }

  /* Remove aerosols with no radiative properties for the input spectral range */
  res = remove_useless_aerosols(atm);
  if(res != RES_OK) goto error;

exit:
  return res;
error:
  reset_gas_properties(&atm->gas);
  FOR_EACH(i, 0, darray_aerosol_size_get(&atm->aerosols)) {
    struct aerosol* aerosol = darray_aerosol_data_get(&atm->aerosols)+i;
    reset_aerosol_properties(aerosol);
  }
  goto exit;
}

res_T
check_properties(const struct rnatm* atm)
{
  size_t i;
  res_T res = RES_OK;
  ASSERT(atm);

  res = check_gas_temperatures(atm);
  if(res != RES_OK) goto error;

  FOR_EACH(i, 0, darray_aerosol_size_get(&atm->aerosols)) {
    const struct aerosol* aerosol = darray_aerosol_cdata_get(&atm->aerosols)+i;
    res = check_aerosol_phase_fn_ids(atm, aerosol);
    if(res != RES_OK) goto error;
  }

exit:
  return res;
error:
  goto exit;
}
