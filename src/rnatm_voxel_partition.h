/* Copyright (C) 2022, 2023 Centre National de la Recherche Scientifique
 * Copyright (C) 2022, 2023 Institut Pierre-Simon Laplace
 * Copyright (C) 2022, 2023 Institut de Physique du Globe de Paris
 * Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2022, 2023 Observatoire de Paris
 * Copyright (C) 2022, 2023 Université de Reims Champagne-Ardenne
 * Copyright (C) 2022, 2023 Université de Versaille Saint-Quentin
 * Copyright (C) 2022, 2023 Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef RNATM_VOXEL_PARTITION_H
#define RNATM_VOXEL_PARTITION_H

#include "rnatm_voxel.h"

#include <rsys/list.h>
#include <rsys/rsys.h>

struct pool_create_args {
  size_t partition_definition; /* #voxels along XYZ. Must be a power of 2 */

  size_t npartitions; /* Overall number of partitions managed by the pool */
  size_t voxel_width; /* Number of items for each voxel data */

  /* Number of pre-allocated partitions must be <= npartitions */
  size_t npreallocated_partitions;

  struct mem_allocator* allocator; /* NULL <=> default allocator */
};
#define POOL_CREATE_ARGS_DEFAULT__ {0, 32, 1, 32, NULL}
static const struct pool_create_args POOL_CREATE_ARGS_DEFAULT =
  POOL_CREATE_ARGS_DEFAULT__;

/******************************************************************************
 * Partition of voxels, i.e. subset of voxels
 ******************************************************************************/
struct partition;

extern LOCAL_SYM void
partition_free
  (struct partition* partition);

/* Commit the partitition, i.e. it can be fetched from the pool up to N times
 * where N is defined by ref_count */
extern LOCAL_SYM void
partition_commit
  (struct partition* partition,
   const size_t ref_count);

extern LOCAL_SYM size_t
partition_get_id
  (const struct partition* partition);

extern LOCAL_SYM size_t
partition_get_definition
  (const struct partition* partition);

/* Empty the partition, that is, its voxels are freed. Therefore, the
 * partition_get_voxel function can no longer be called; you have to use the
 * partition_cget_voxel function instead */
extern LOCAL_SYM void
partition_empty
  (struct partition* partition);

extern LOCAL_SYM float*
partition_get_voxel
  (struct partition* partition,
   const size_t ivoxel,
   const size_t iitem);

/* Returns a voxel even if the partition is empty. In the latter case, it
 * always returns an empty voxel */
extern LOCAL_SYM const float*
partition_cget_voxel
  (struct partition* partition,
   const size_t ivoxel,
   const size_t iitem);

extern LOCAL_SYM void
partition_clear_voxels
  (struct partition* partition);

extern LOCAL_SYM void
partition_accum
  (struct partition* dst,
   struct partition* src);

/******************************************************************************
 * Partition pool, i.e. collection of partitions of voxels that are accessible
 * concurrently by several threads
 ******************************************************************************/
struct pool;

extern LOCAL_SYM res_T
pool_create
  (const struct pool_create_args* args,
   struct pool** pool);

extern LOCAL_SYM void
pool_ref_get
  (struct pool* pool);

extern LOCAL_SYM void
pool_ref_put
  (struct pool* pool);

extern LOCAL_SYM size_t
pool_get_partition_definition
  (const struct pool* pool);

extern LOCAL_SYM size_t
pool_get_voxel_width
  (const struct pool* pool);

/* Returns a free partition. Waits for a free partition to be available.
 * Returns NULL if an error occurs */
extern LOCAL_SYM struct partition*
pool_next_partition
  (struct pool* pool);

/* Same as pool_next_partition but the returned partition has no ID (it
 * cannot be commited). Such a partition is used as a temporary variable */
extern LOCAL_SYM struct partition*
pool_dummy_partition
  (struct pool* pool);

/* Returns the partition with the identifier 'ipartition'. Waits for the
 * partition to be available. Returns NULL if an error occurs */
extern LOCAL_SYM struct partition*
pool_fetch_partition
  (struct pool* pool,
   const size_t ipartition);

/* Makes the pool invalid. Once invalidated, the 'next' and 'fetch'
 * functions return NULL */
extern LOCAL_SYM void
pool_invalidate
  (struct pool* pool);

/* Reset the next partition to fetch to 0 */
extern LOCAL_SYM void
pool_reset
  (struct pool* pool);

#endif /* RNATM_VOXEL_PARTITION_H */
