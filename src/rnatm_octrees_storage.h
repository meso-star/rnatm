/* Copyright (C) 2022, 2023 Centre National de la Recherche Scientifique
 * Copyright (C) 2022, 2023 Institut Pierre-Simon Laplace
 * Copyright (C) 2022, 2023 Institut de Physique du Globe de Paris
 * Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2022, 2023 Observatoire de Paris
 * Copyright (C) 2022, 2023 Université de Reims Champagne-Ardenne
 * Copyright (C) 2022, 2023 Université de Versaille Saint-Quentin
 * Copyright (C) 2022, 2023 Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef RNATM_OCTREES_STORAGE_H
#define RNATM_OCTREES_STORAGE_H

#include <rsys/hash.h>
#include <rsys/rsys.h>

struct rnatm;

/* Storage version. It must be incremented and versioning must be performed on
 * serialized data when its memory layout is updated */
static const int OCTREES_STORAGE_VERSION = 1;

struct octrees_storage_desc {
  size_t ibands[2]; /* Indices of the band to handle. Limits are inclusive */
  double optical_thickness; /* Threshold used during octree construction */
  unsigned grid_definition[3];

  /* Signature of the meshes used to build the octrees */
  hash256_T mesh_signature;
  hash256_T* band_signatures; /* Per band signature */

  struct mem_allocator* allocator;
  int version;
};
#define OCTREES_STORAGE_DESC_NULL__ {{0,0},0,{0,0,0},{0},NULL,NULL,0}
static const struct octrees_storage_desc OCTREES_STORAGE_DESC_NULL =
  OCTREES_STORAGE_DESC_NULL__;

extern LOCAL_SYM res_T
octrees_storage_desc_init
  (const struct rnatm* atm,
   struct octrees_storage_desc* desc);

extern LOCAL_SYM res_T
octrees_storage_desc_init_from_stream
  (const struct rnatm* atm,
   struct octrees_storage_desc* desc,
   FILE* stream);

extern LOCAL_SYM void
octrees_storage_desc_release
  (struct octrees_storage_desc* desc);

extern LOCAL_SYM res_T
check_octrees_storage_compatibility
  (const struct octrees_storage_desc* reference,
   const struct octrees_storage_desc* challenge);

extern LOCAL_SYM res_T
write_octrees_storage_desc
  (const struct rnatm* atm,
   const struct octrees_storage_desc* desc,
   FILE* stream);

extern LOCAL_SYM res_T
reserve_octrees_storage_toc
  (const struct rnatm* atm,
   FILE* stream);

extern LOCAL_SYM res_T
write_octrees_storage_toc
  (const struct rnatm* atm,
   FILE* stream);

extern LOCAL_SYM res_T
read_octrees_storage_toc
  (struct rnatm* atm,
   FILE* stream);

extern LOCAL_SYM res_T
store_octrees
  (struct rnatm* atm,
   const size_t ioctrees[2],
   FILE* stream);

#endif /* RNATM_OCTREES_STORAGE_H */
