/* Copyright (C) 2022, 2023 Centre National de la Recherche Scientifique
 * Copyright (C) 2022, 2023 Institut Pierre-Simon Laplace
 * Copyright (C) 2022, 2023 Institut de Physique du Globe de Paris
 * Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2022, 2023 Observatoire de Paris
 * Copyright (C) 2022, 2023 Université de Reims Champagne-Ardenne
 * Copyright (C) 2022, 2023 Université de Versaille Saint-Quentin
 * Copyright (C) 2022, 2023 Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200809L /* strdup, strtok_r */

#include "rnatm.h"

#include <rsys/cstr.h>
#include <rsys/mem_allocator.h>
#include <rsys/rsys.h>
#include <rsys/stretchy_array.h>

#include <getopt.h>
#include <string.h>

struct args {
  struct rnatm_create_args rnatm;
  const char* vtk_filename;
  int check;
  int quit;
};
#define ARGS_DEFAULT__ { RNATM_CREATE_ARGS_DEFAULT__, NULL, 0, 0 }
static const struct args ARGS_DEFAULT = ARGS_DEFAULT__;

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
print_help(const char* cmd)
{
  ASSERT(cmd);
  printf(
"Usage: %s -g gas\n"
"Test the Rad-Net ATMosphere library\n\n",
    cmd);
  printf(
"  -a aerosol     atmospheric aerosol. Use this option as many times as\n"
"                 there are aerosols to declare. An aerosol is defined\n"
"                 by the following parameters, each parameter is\n"
"                 separated from the previous one by a colon (:)\n\n");
  printf(
"                   mesh=path       aerosol mesh (smsh(5))\n"
"                   name=string     aerosol name\n"
"                   radprop=path    radiative properties (sars(5))\n"
"                   phasefn=path    list of phase functions (rnsl(5))\n"
"                   phaseids=path   phase function id by node (rnpfi(5))\n\n");
  printf(
"  -c             check data\n");
  printf(
"  -d file        write the builded octrees to file according to the VTK\n"
"                 file format. The octrees are written to standard ouput\n"
"                 if the file is a dash (-). To split the resulting file\n"
"                 into n VTK files, each storing an octree, one can use\n"
"                 the csplit command. For example with n = 4:\n\n");
  printf(
"                   csplit -f octree -k file %%^#\\ vtk%% /^#\\ vtk/ {2}\n\n");
  printf(
"  -g gas         atmospheric gas mixture. This mixture is defined by\n"
"                 the following parameters, each parameter is separated\n"
"                 from the previous one by a colon (:)\n\n");
  printf(
"                   mesh=path       gas mesh (smsh(5))\n"
"                   ck=path         correlated-K of the mixture (sck(5))\n"
"                   temp=path       temperatures (rngt(5))\n\n");
  printf(
"  -h             display this help and exit\n");
  printf(
"  -i file        octrees are loaded from file\n");
  printf(
"  -N             precompute_normals the tetrahedra normals\n");
  printf(
"  -n             atmosphere name. Default is `%s'\n",
    ARGS_DEFAULT.rnatm.name);
  printf(
"  -o file        offload octrees to file\n");
  printf(
"  -s nu0,nu1     spectral range to consider (in nm).\n"
"                 Default is visible spectrum, i.e [%g, %g] nm\n",
    ARGS_DEFAULT.rnatm.spectral_range[0],
    ARGS_DEFAULT.rnatm.spectral_range[1]);
  printf(
"  -T threshold   optical thickness criteria for octree building.\n"
"                 Default is %g\n",
    ARGS_DEFAULT.rnatm.optical_thickness);
  printf(
"  -t nthreads    hint on the number of threads. Default assumes\n"
"                 as many threads as CPU cores\n");
  printf(
"  -V definition  advice on the definiton of the acceleration\n"
"                 structure along the 3 axes. Default is %u\n",
    ARGS_DEFAULT.rnatm.grid_definition_hint);
  printf(
"  -v             make the program verbose\n");
  printf("\n");
  printf(
"This is free software released under the GNU GPL license, version 3 or\n"
"later. You are free to change or redistribute it under certain\n"
"conditions <http://gnu.org.licenses/gpl.html>\n");
}

static res_T
parse_gas_parameters(const char* str, void* ptr)
{
  enum { MESH, CK, TEMP } iparam;
  char buf[BUFSIZ];
  struct args* args = ptr;
  char* key;
  char* val;
  char* tk_ctx;
  res_T res = RES_OK;
  ASSERT(args && str);

  if(strlen(str) >= sizeof(buf) -1/*NULL char*/) {
    fprintf(stderr, "Could not duplicate the gas parameter `%s'\n", str);
    res = RES_MEM_ERR;
    goto error;
  }
  strncpy(buf, str, sizeof(buf));

  key = strtok_r(buf, "=", &tk_ctx);
  val = strtok_r(NULL, "", &tk_ctx);

       if(!strcmp(key, "mesh")) iparam = MESH;
  else if(!strcmp(key, "ck")) iparam = CK;
  else if(!strcmp(key, "temp")) iparam = TEMP;
  else {
    fprintf(stderr, "Invalid gas parameter `%s'\n", key);
    res = RES_BAD_ARG;
    goto error;
  }

  if(!val) {
    fprintf(stderr, "Invalid null value for gas parameter `%s'\n", key);
    res = RES_BAD_ARG;
    goto error;
  }

  switch(iparam) {
    case MESH:
      args->rnatm.gas.smsh_filename = strdup(val);
      if(!args->rnatm.gas.smsh_filename) res = RES_MEM_ERR;
      break;
    case CK:
      args->rnatm.gas.sck_filename = strdup(val);
      if(!args->rnatm.gas.sck_filename) res = RES_MEM_ERR;
      break;
    case TEMP:
      args->rnatm.gas.temperatures_filename = strdup(val);
      if(!args->rnatm.gas.temperatures_filename) res = RES_MEM_ERR;
      break;
    default: FATAL("Unreachable code\n"); break;
  }
  if(res != RES_OK) {
    fprintf(stderr, "Unable to parse the gas parameter `%s' -- %s\n",
      str, res_to_cstr(res));
    goto error;
  }

exit:
  return res;
error:
  goto exit;
}

static res_T
parse_aerosol_parameters(const char* str, void* ptr)
{
  enum { MESH, NAME, RADPROP, PHASEFN, PHASEIDS } iparam;
  struct rnatm_aerosol_args* aerosol = NULL;
  char buf[BUFSIZ];
  struct args* args = ptr;
  char* key;
  char* val;
  char* tk_ctx;
  res_T res = RES_OK;
  ASSERT(args && str);

  if(strlen(str) >= sizeof(buf) -1/*NULL char*/) {
    fprintf(stderr, "Could not duplicate the aerosol parameter `%s'\n", str);
    res = RES_MEM_ERR;
    goto error;
  }
  strncpy(buf, str, sizeof(buf));

  key = strtok_r(buf, "=", &tk_ctx);
  val = strtok_r(NULL, "", &tk_ctx);

       if(!strcmp(key, "mesh")) iparam = MESH;
  else if(!strcmp(key, "name")) iparam = NAME;
  else if(!strcmp(key, "radprop")) iparam = RADPROP;
  else if(!strcmp(key, "phasefn")) iparam = PHASEFN;
  else if(!strcmp(key, "phaseids")) iparam = PHASEIDS;
  else {
    fprintf(stderr, "Invalid aerosol parameter `%s'\n", key);
    res = RES_BAD_ARG;
    goto error;
  }

  if(!val) {
    fprintf(stderr, "Invalid null value for aerosol parameter `%s'\n", key);
    res = RES_BAD_ARG;
    goto error;
  }

  ASSERT(args->rnatm.naerosols);
  aerosol = args->rnatm.aerosols + (args->rnatm.naerosols - 1);

  switch(iparam) {
    case MESH:
      aerosol->smsh_filename = strdup(val);
      if(!aerosol->smsh_filename) res = RES_MEM_ERR;
      break;
    case NAME:
      aerosol->name = strdup(val);
      if(!aerosol->name) res = RES_MEM_ERR;
      break;
    case RADPROP:
      aerosol->sars_filename = strdup(val);
      if(!aerosol->sars_filename) res = RES_MEM_ERR;
      break;
    case PHASEFN:
      aerosol->phase_fn_lst_filename = strdup(val);
      if(!aerosol->phase_fn_lst_filename) res = RES_MEM_ERR;
      break;
    case PHASEIDS:
      aerosol->phase_fn_ids_filename = strdup(val);
      if(!aerosol->phase_fn_ids_filename) res = RES_MEM_ERR;
      break;
    default: FATAL("Unreachable code\n"); break;
  }
  if(res != RES_OK) {
    fprintf(stderr, "Unable to parse the aerosol parameter `%s' -- %s\n",
      str, res_to_cstr(res));
    goto error;
  }

exit:
  return res;
error:
  goto exit;
}

static res_T
parse_spectral_range(struct args* args, char* str)
{
  size_t len = 0;
  res_T res = RES_OK;
  ASSERT(args && str);

  res = cstr_to_list_double(str, ',', args->rnatm.spectral_range, &len, 2);
  if(res == RES_OK && len != 2) res = RES_BAD_ARG;
  if(res != RES_OK) goto error;

  if(args->rnatm.spectral_range[0] < 0
  || args->rnatm.spectral_range[1] < 0
  || args->rnatm.spectral_range[0] > args->rnatm.spectral_range[1])
    goto error;

exit:
  return res;
error:
  goto exit;
}

static void
args_release(struct args* args)
{
  size_t i;
  ASSERT(args);
  if(args->rnatm.gas.smsh_filename) free(args->rnatm.gas.smsh_filename);
  if(args->rnatm.gas.sck_filename) free(args->rnatm.gas.sck_filename);
  if(args->rnatm.gas.temperatures_filename)
    free(args->rnatm.gas.temperatures_filename);
  if(args->rnatm.octrees_storage) CHK(fclose(args->rnatm.octrees_storage) == 0);

  FOR_EACH(i, 0, args->rnatm.naerosols) {
    struct rnatm_aerosol_args* aerosol = args->rnatm.aerosols + i;
    if(aerosol->name) free(aerosol->name);
    if(aerosol->smsh_filename) free(aerosol->smsh_filename);
    if(aerosol->sars_filename) free(aerosol->sars_filename);
    if(aerosol->phase_fn_ids_filename) free(aerosol->phase_fn_ids_filename);
    if(aerosol->phase_fn_lst_filename) free(aerosol->phase_fn_lst_filename);
  }
  sa_release(args->rnatm.aerosols);
  *args = ARGS_DEFAULT;
}

static res_T
args_init(struct args* args, int argc, char** argv)
{
  const char* storage_filename = NULL;
  size_t i = 0;
  res_T res = RES_OK;
  int opt;
  ASSERT(args && argc && argv);

  *args = ARGS_DEFAULT;

  while((opt = getopt(argc, argv, "a:cd:g:hi:Nn:o:s:T:t:V:v")) != -1) {
    switch(opt) {
      case 'a':
        (void)sa_add(args->rnatm.aerosols, 1);
        args->rnatm.aerosols[args->rnatm.naerosols] = RNATM_AEROSOL_ARGS_NULL;
        args->rnatm.naerosols += 1;
        res = cstr_parse_list(optarg, ':', parse_aerosol_parameters, args);
        break;
      case 'c': args->check = 1; break;
      case 'd': args->vtk_filename = optarg; break;
      case 'g':
        res = cstr_parse_list(optarg, ':', parse_gas_parameters, args);
        break;
      case 'h':
        print_help(argv[0]);
        args_release(args);
        args->quit = 1;
        goto exit;
      case 'N': args->rnatm.precompute_normals = 1; break;
      case 'n': args->rnatm.name = optarg; break;
      case 'o':
        args->rnatm.load_octrees_from_storage = 0;
        storage_filename = optarg;
        break;
      case 'i':
        args->rnatm.load_octrees_from_storage = 1;
        storage_filename = optarg;
        break;
      case 's':
        res = parse_spectral_range(args, optarg);
        break;
      case 'T':
        res = cstr_to_double(optarg, &args->rnatm.optical_thickness);
        if(res != RES_OK && args->rnatm.optical_thickness<=0) res = RES_BAD_ARG;
        break;
      case 't':
        res = cstr_to_uint(optarg, &args->rnatm.nthreads);
        if(res == RES_OK && !args->rnatm.nthreads) res = RES_BAD_ARG;
        break;
      case 'V':
        res = cstr_to_uint(optarg, &args->rnatm.grid_definition_hint);
        if(res == RES_OK && !args->rnatm.grid_definition_hint) res = RES_BAD_ARG;
        break;
      case 'v': args->rnatm.verbose = 1; break;
      default: res = RES_BAD_ARG; break;
    }
    if(res != RES_OK) {
      if(optarg) {
        fprintf(stderr, "%s: invalid option args `%s' -- `%c'\n",
          argv[0], optarg, opt);
      }
      goto error;
    }
  }

  if(storage_filename) {
    const char* mode = args->rnatm.load_octrees_from_storage ? "r" : "w+";
    args->rnatm.octrees_storage = fopen(storage_filename, mode);
    if(!args->rnatm.octrees_storage) {
      fprintf(stderr, "Unable to open octree storage file %s\n",
        storage_filename);
      res = RES_IO_ERR;
      goto error;
    }
  }

  /* Check the required options */
  if(!args->rnatm.gas.smsh_filename
  || !args->rnatm.gas.sck_filename
  || !args->rnatm.gas.temperatures_filename) {
    fprintf(stderr, "Incomplete gas definition -- option `-g'\n");
    res = RES_BAD_ARG;
    goto error;
  }

  FOR_EACH(i, 0, args->rnatm.naerosols) {
    struct rnatm_aerosol_args* aerosol = args->rnatm.aerosols + i;

    if(!aerosol->smsh_filename
    || !aerosol->sars_filename
    || !aerosol->phase_fn_ids_filename
    || !aerosol->phase_fn_lst_filename) {
      fprintf(stderr, "Incomplete %lu^th aerosol definition -- option `-a'\n",
        (unsigned long)i);
      res = RES_BAD_ARG;
      goto error;
    }
  }

exit:
  return res;
error:
  args_release(args);
  goto exit;
}

static res_T
write_vtk_octrees(struct rnatm* atm, const char* filename)
{
  size_t octrees_range[2];
  FILE* fp = NULL;
  res_T res = RES_OK;
  ASSERT(atm && filename);

  if(!strcmp(filename, "-")) {
    fp = stdout;
  } else {
    fp = fopen(filename, "w");
    if(!fp) {
      fprintf(stderr, "Could not open `%s' -- %s\n", filename, strerror(errno));
      res = RES_IO_ERR;
      goto error;
    }
  }

  octrees_range[0] = 0;
  octrees_range[1] = rnatm_get_spectral_items_count(atm) - 1;

  res = rnatm_write_vtk_octrees(atm, octrees_range, fp);
  if(res != RES_OK) goto error;

exit:
  if(fp != stdout) CHK(fclose(fp) == 0);
  return res;
error:
  goto exit;
}

/*******************************************************************************
 * Main function
 ******************************************************************************/
int
main(int argc, char** argv)
{
  struct args  args = ARGS_DEFAULT;
  struct rnatm* atm = NULL;
  res_T res = RES_OK;
  int err = 0;

  res = args_init(&args, argc, argv);
  if(res != RES_OK) goto error;

  res = rnatm_create(&args.rnatm, &atm);
  if(res != RES_OK) goto error;

  if(args.check) {
    res = rnatm_validate(atm);
    if(res != RES_OK) goto error;
  }

  if(args.vtk_filename) {
    res = write_vtk_octrees(atm, args.vtk_filename);
    if(res != RES_OK) goto error;
  }

exit:
  args_release(&args);
  if(atm) RNATM(ref_put(atm));
  if(mem_allocated_size() != 0) {
    fprintf(stderr, "Memory leaks: %lu bytes\n",
      (unsigned long)mem_allocated_size());
    err = -1;
  }
  return err;
error:
  err = -1;
  goto exit;
}
