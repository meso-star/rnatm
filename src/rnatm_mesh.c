/* Copyright (C) 2022, 2023 Centre National de la Recherche Scientifique
 * Copyright (C) 2022, 2023 Institut Pierre-Simon Laplace
 * Copyright (C) 2022, 2023 Institut de Physique du Globe de Paris
 * Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2022, 2023 Observatoire de Paris
 * Copyright (C) 2022, 2023 Université de Reims Champagne-Ardenne
 * Copyright (C) 2022, 2023 Université de Versaille Saint-Quentin
 * Copyright (C) 2022, 2023 Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "rnatm.h"
#include "rnatm_c.h"
#include "rnatm_log.h"

#include <star/smsh.h>
#include <star/suvm.h>

#include <rsys/clock_time.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static res_T
check_smsh_desc(struct rnatm* atm, const struct smsh_desc* desc)
{
  res_T res = RES_OK;
  ASSERT(atm && desc);

  if(desc->dnode != 3 || desc->dcell != 4) {
    log_err(atm,
      "An atmosphere mesh must be a 3D tetrahedral mesh "
      "(dimension of the mesh: %u; dimension of the vertices: %u)\n",
      desc->dnode, desc->dcell);
    res = RES_BAD_ARG;
    goto error;
  }

exit:
  return res;
error:
  goto exit;
}

static INLINE void
tetrahedron_get_indices(const size_t itetra, size_t ids[4], void* ctx)
{
  const struct smsh_desc* desc = ctx;
  const uint64_t* indices = NULL;
  ASSERT(ctx && ids && itetra < desc->ncells && desc->dcell == 4);
  indices = smsh_desc_get_cell(desc, itetra);
  ids[0] = (size_t)indices[0];
  ids[1] = (size_t)indices[1];
  ids[2] = (size_t)indices[2];
  ids[3] = (size_t)indices[3];
}

static INLINE void
vertex_get_position(const size_t ivert, double pos[3], void* ctx)
{
  struct smsh_desc* desc = ctx;
  const double* position = NULL;
  ASSERT(ctx && pos && ivert < desc->nnodes && desc->dnode == 3);
  position = smsh_desc_get_node(desc, ivert);
  pos[0] = position[0];
  pos[1] = position[1];
  pos[2] = position[2];
}

static res_T
setup_uvm
  (struct rnatm* atm,
   const struct rnatm_create_args* args,
   const char* filename,
   struct suvm_device* suvm,
   struct smsh* smsh,
   struct suvm_volume** out_volume,
   size_t* ntetrahedra,
   size_t* nvertices)
{
  struct suvm_tetrahedral_mesh_args mesh_args = SUVM_TETRAHEDRAL_MESH_ARGS_NULL;
  struct smsh_load_args smsh_load_args = SMSH_LOAD_ARGS_NULL;
  struct smsh_desc smsh_desc = SMSH_DESC_NULL;
  struct suvm_volume* volume = NULL;
  res_T res = RES_OK;
  ASSERT(atm && args && filename && suvm && smsh && out_volume);

  /* Load and retrieve the Star-Mesh data */
  smsh_load_args.path = filename;
  smsh_load_args.memory_mapping = 0;
  res = smsh_load(smsh, &smsh_load_args);
  if(res != RES_OK) goto error;
  res = smsh_get_desc(smsh, &smsh_desc);
  if(res != RES_OK) goto error;
  res = check_smsh_desc(atm, &smsh_desc);
  if(res != RES_OK) goto error;

  /* Partition the unstructured volumetric mesh */
  mesh_args.ntetrahedra = smsh_desc.ncells;
  mesh_args.nvertices = smsh_desc.nnodes;
  mesh_args.get_indices = tetrahedron_get_indices;
  mesh_args.get_position = vertex_get_position;
  mesh_args.tetrahedron_data = SUVM_DATA_NULL; /* Tetra data are not in SUVM */
  mesh_args.vertex_data = SUVM_DATA_NULL; /* Vertex data are not in SUVM */
  mesh_args.precompute_normals = args->precompute_normals;
  mesh_args.context = &smsh_desc;
  res = suvm_tetrahedral_mesh_create(suvm, &mesh_args, &volume);
  if(res != RES_OK) goto error;

exit:
  *out_volume = volume;
  *ntetrahedra = smsh_desc.ncells;
  *nvertices = smsh_desc.nnodes;
  return res;
error:
  if(volume) { SUVM(volume_ref_put(volume)); volume = NULL; }
  goto exit;
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
rnatm_fetch_cell
  (const struct rnatm* atm,
   const double pos[3],
   const size_t cpnt,
   struct rnatm_cell_pos* cell)
{
  res_T res = RES_OK;

  if(!atm || !pos || !cell) {
    res = RES_BAD_ARG;
    goto error;
  }

  cell->component = cpnt;

  /* Gas */
  if(cpnt == RNATM_GAS) {
    res = suvm_volume_at
      (atm->gas.volume, pos, &cell->prim, cell->barycentric_coords);
    if(res != RES_OK) {
      log_err(atm, "Error retrieving gas cell at %g, %g, %g\n",
        SPLIT3(pos));
      goto error;
    }

  /* Aerosol */
  } else if(cpnt < rnatm_get_aerosols_count(atm)) {
    const struct aerosol* aerosol = darray_aerosol_cdata_get(&atm->aerosols) + cpnt;
    res = suvm_volume_at
      (aerosol->volume, pos, &cell->prim, cell->barycentric_coords);
    if(res != RES_OK) {
      log_err(atm, "Error retrieving %s cell at %g, %g, %g\n",
        str_cget(&aerosol->name), SPLIT3(pos));
      goto error;
    }

  /* Invalid component */
  } else {
    res = RES_BAD_ARG;
    goto error;
  }

exit:
  return res;
error:
  goto exit;
}

res_T
rnatm_fetch_cell_list
  (const struct rnatm* atm,
   const double pos[3],
   struct rnatm_cell_pos* cells,
   size_t* ncells)
{
  size_t cpnt = RNATM_GAS;
  size_t naerosols = 0;
  size_t i = 0;
  res_T res = RES_OK;

  if(!atm || !pos || !cells) {
    res = RES_BAD_ARG;
    goto error;
  }

  naerosols = darray_aerosol_size_get(&atm->aerosols);
  ASSERT(naerosols+1/*gas*/ < RNATM_MAX_COMPONENTS_COUNT);

  do {
    res = rnatm_fetch_cell(atm, pos, cpnt, cells+i);
    if(res != RES_OK) goto error;

    ++i;

  } while(++cpnt < naerosols);
  ASSERT(i == naerosols+1);

  if(ncells) *ncells = naerosols + 1;

exit:
  return res;
error:
  goto exit;
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
res_T
setup_meshes(struct rnatm* atm, const struct rnatm_create_args* args)
{
  char buf[128];
  struct time t0, t1;
  double gas_low[3];
  double gas_upp[3];
  struct suvm_device* suvm = NULL;
  struct smsh_create_args smsh_args = SMSH_CREATE_ARGS_DEFAULT;
  struct smsh* smsh = NULL;
  size_t i;
  res_T res = RES_OK;
  ASSERT(atm && args);

  log_info(atm, "load and structure the atmosphere meshes\n");
  time_current(&t0);

  /* Create the Star-UVM device */
  res = suvm_device_create(atm->logger, atm->allocator, atm->verbose, &suvm);
  if(res != RES_OK) goto error;

  /* Create the Star-Mesh loader */
  smsh_args.logger = atm->logger;
  smsh_args.allocator = atm->allocator;
  smsh_args.verbose = atm->verbose;
  res = smsh_create(&smsh_args, &smsh);
  if(res != RES_OK) goto error;

  /* Load and structure gas volumetric mesh */
  log_info(atm, "gas mesh: %s\n", args->gas.smsh_filename);
  res = setup_uvm(atm, args, args->gas.smsh_filename, suvm, smsh,
    &atm->gas.volume, &atm->gas.ntetrahedra, &atm->gas.nvertices);
  if(res != RES_OK) goto error;
  res = suvm_volume_get_aabb(atm->gas.volume, gas_low, gas_upp);
  if(res != RES_OK) goto error;

  /* Load and structure aerosol volumetric meshes */
  FOR_EACH(i, 0, args->naerosols) {
    double aerosol_low[3];
    double aerosol_upp[3];
    struct aerosol* aerosol = darray_aerosol_data_get(&atm->aerosols)+i;
    const char* aerosol_name = str_cget(&aerosol->name);
    const char* filename = args->aerosols[i].smsh_filename;

    /* Load and structure the aerosol mesh */
    log_info(atm, "%s mesh: %s\n", aerosol_name, filename);
    res = setup_uvm(atm, args, filename, suvm, smsh, &aerosol->volume,
      &aerosol->ntetrahedra, &aerosol->nvertices);
    if(res != RES_OK) goto error;
    res = suvm_volume_get_aabb(aerosol->volume, aerosol_low, aerosol_upp);
    if(res != RES_OK) goto error;

    /* Check that the aerosol is included in the gas */
    if(gas_low[0] > aerosol_low[0]
    || gas_low[1] > aerosol_low[1]
    || gas_low[2] > aerosol_low[2]
    || gas_upp[0] < aerosol_upp[0]
    || gas_upp[1] < aerosol_upp[1]
    || gas_upp[2] < aerosol_upp[2]) {
      log_err(atm,
        "The %s may not be included in the gas "
        "(gas AABB: {%g, %g, %g} - {%g, %g, %g}; "
        "%s AABB: {%g, %g, %g} - {%g, %g, %g})\n",
        aerosol_name,
        SPLIT3(gas_low),
        SPLIT3(gas_upp),
        aerosol_name,
        SPLIT3(aerosol_low),
        SPLIT3(aerosol_upp));
      res = RES_BAD_ARG;
      goto error;
    }
  }

  /* Dump elapsed time */
  time_sub(&t0, time_current(&t1), &t0);
  time_dump(&t0, TIME_ALL, NULL, buf, sizeof(buf));
  log_info(atm, "atmopshere meshes setuped in %s\n", buf);

exit:
  if(smsh) SMSH(ref_put(smsh));
  if(suvm) SUVM(device_ref_put(suvm));
  return res;
error:
  if(atm->gas.volume) {
    SUVM(volume_ref_put(atm->gas.volume));
    atm->gas.volume = NULL;
  }
  FOR_EACH(i, 0, args->naerosols) {
    struct aerosol* aerosol = darray_aerosol_data_get(&atm->aerosols)+i;
    if(aerosol->volume) {
      SUVM(volume_ref_put(aerosol->volume));
      aerosol->volume = NULL;
    }
  }
  goto exit;
}
