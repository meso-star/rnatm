/* Copyright (C) 2022, 2023 Centre National de la Recherche Scientifique
 * Copyright (C) 2022, 2023 Institut Pierre-Simon Laplace
 * Copyright (C) 2022, 2023 Institut de Physique du Globe de Paris
 * Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2022, 2023 Observatoire de Paris
 * Copyright (C) 2022, 2023 Université de Reims Champagne-Ardenne
 * Copyright (C) 2022, 2023 Université de Versaille Saint-Quentin
 * Copyright (C) 2022, 2023 Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef RNATM_H
#define RNATM_H

#include <star/suvm.h>
#include <star/svx.h>

#include <rsys/rsys.h>

/* Library symbol management */
#if defined(RNATM_SHARED_BUILD) /* Build shared library */
  #define RNATM_API extern EXPORT_SYM
#elif defined(RNATM_STATIC) /* Use/build static library */
  #define RNATM_API extern LOCAL_SYM
#else /* Use shared library */
  #define RNATM_API extern IMPORT_SYM
#endif

/* Helper macro that asserts if the invocation of the rnatm function `Func'
 * returns an error. One should use this macro on suvm function calls for
 * which no explicit error checking is performed */
#ifndef NDEBUG
  #define RNATM(Func) ASSERT(rnatm_ ## Func == RES_OK)
#else
  #define RNATM(Func) rnatm_ ## Func
#endif

/* Gas identifier */
#define RNATM_GAS ((size_t)-1)

/* Maximum number of components supported by the library (gas included) */
#define RNATM_MAX_COMPONENTS_COUNT 16

/* Forward declaration of external data types */
struct logger;
struct mem_allocator;
struct ssf_phase;

enum rnatm_radcoef {
  RNATM_RADCOEF_Ka, /* Absorption coefficient */
  RNATM_RADCOEF_Ks, /* Scattering coefficient */
  RNATM_RADCOEF_Kext, /* Extinction coefficient */
  RNATM_RADCOEFS_COUNT__
};

enum rnatm_svx_op {
  RNATM_SVX_OP_MIN,
  RNATM_SVX_OP_MAX,
  RNATM_SVX_OPS_COUNT__
};

struct rnatm_gas_args {
  char* smsh_filename; /* Geometry */
  char* sck_filename; /* Radiative properties */
  char* temperatures_filename; /* Temperature */
};
#define RNATM_GAS_ARGS_NULL__ {NULL, NULL, NULL}
static const struct rnatm_gas_args RNATM_GAS_ARGS_NULL = RNATM_GAS_ARGS_NULL__;

struct rnatm_aerosol_args {
  char* name; /* NULL <=> use default name */
  char* smsh_filename; /* Geometry */
  char* sars_filename; /* Radiative properties */
  char* phase_fn_ids_filename; /* Per node phase function id */
  char* phase_fn_lst_filename; /* List of phase functions */
};
#define RNATM_AEROSOL_ARGS_NULL__ {NULL, NULL, NULL, NULL, NULL}
static const struct rnatm_aerosol_args RNATM_AEROSOL_ARGS_NULL =
  RNATM_AEROSOL_ARGS_NULL__;

struct rnatm_cell_pos {
  struct suvm_primitive prim; /* Volumetric primitive */
  double barycentric_coords[4]; /* position relative to `prim' */

  /* Component to which the cell belongs. This is either an aerosol index or
   * the RNATM_GAS constant  */
  size_t component;
};
#define RNATM_CELL_POS_NULL__ {SUVM_PRIMITIVE_NULL__, {0,0,0,0}, RNATM_GAS}
static const struct rnatm_cell_pos RNATM_CELL_POS_NULL = RNATM_CELL_POS_NULL__;

struct rnatm_band_desc {
  double lower; /* Lower band wavelength in nm (inclusive) */
  double upper; /* Upper band wavelength in nm (exclusive) */
  size_t quad_pts_count; /* #quadrature points */
};
#define RNATM_BAND_DESC_NULL__ {0,0,0}
static const struct rnatm_band_desc RNATM_BAND_DESC_NULL =
  RNATM_BAND_DESC_NULL__;

struct rnatm_get_radcoef_args {
  /* Cells to be queried. Refer to the rnatm_fetch_cell_list function */
  struct rnatm_cell_pos* cells;

  size_t iband; /* Index of the spectral band to consider */
  size_t iquad; /* Index of the quadrature point to consider */

  enum rnatm_radcoef radcoef;

  /* For debug: check that the retrieved radcoef is in [k_min, k_max] */
  double k_min;
  double k_max;
};
#define RNATM_GET_RADCOEF_ARGS_NULL__ {                                        \
  NULL, 0, 0, RNATM_RADCOEFS_COUNT__, -DBL_MAX, DBL_MAX                        \
}
static const struct rnatm_get_radcoef_args
RNATM_GET_RADCOEF_ARGS_NULL = RNATM_GET_RADCOEF_ARGS_NULL__;

struct rnatm_sample_component_args {
  /* Cells to be queried. Refer to the rnatm_fetch_cell_list function */
  const struct rnatm_cell_pos* cells;

  size_t iband; /* Index of the spectral band to consider */
  size_t iquad; /* Index of the quadrature point to consider */

  enum rnatm_radcoef radcoef;

  double r; /* Random number uniformaly distributed in [0, 1[ */
};
#define RNATM_SAMPLE_COMPONENT_ARGS_NULL__ {                                   \
  NULL, 0, 0, RNATM_RADCOEFS_COUNT__, 0                                        \
}
static const struct rnatm_sample_component_args
RNATM_SAMPLE_COMPONENT_ARGS_NULL = RNATM_SAMPLE_COMPONENT_ARGS_NULL__;

struct rnatm_cell_get_radcoef_args {
  struct rnatm_cell_pos cell; /* Cell to query */
  size_t iband; /* Index of the spectral band to query */
  size_t iquad; /* Index of the quadrature point to query in the band */

  enum rnatm_radcoef radcoef;

  /* For debug: check that the retrieved radcoef is < k_max. Do not provide
   * k_min because the radiative coefficient of a component may be less than
   * the radiative coefficient of the mixture */
  double k_max;
};
#define RNATM_CELL_GET_RADCOEF_ARGS_NULL__ {                                   \
  RNATM_CELL_POS_NULL__,                                                       \
  0, 0, /* Spectral data (band, quadrature pointh) */                          \
  RNATM_RADCOEFS_COUNT__,                                                      \
  DBL_MAX /* For debug: Radcoef range */                                       \
}
static const struct rnatm_cell_get_radcoef_args
RNATM_CELL_GET_RADCOEF_ARGS_NULL = RNATM_CELL_GET_RADCOEF_ARGS_NULL__;

struct rnatm_cell_create_phase_fn_args {
  struct rnatm_cell_pos cell; /* Cell to query */
  double wavelength; /* In nm */
  double r[2]; /* Random numbers uniformaly distributed in [0, 1[ */
};
#define RNATM_CELL_CREATE_PHASE_FN_ARGS_NULL__ {                               \
  RNATM_CELL_POS_NULL__,  0, {0,0}                                             \
}
static const struct rnatm_cell_create_phase_fn_args
RNATM_CELL_CREATE_PHASE_FN_ARGS_NULL = RNATM_CELL_CREATE_PHASE_FN_ARGS_NULL__;

struct rnatm_trace_ray_args {
  double ray_org[3]; /* Origin of the ray */
  double ray_dir[3]; /* Direction of the ray */
  double ray_range[2]; /* Range of the ray */

  svx_hit_challenge_T challenge; /* NULL <=> Traversed up to the leaves */
  svx_hit_filter_T filter; /* NULL <=> Stop RT at the 1st intersected voxel */
  void* context; /* User data send to the 'challenge' & 'filter' function */

  size_t iband; /* Index of the spectral band to consider */
  size_t iquad; /* Index of the quadrature point to consider */
};
#define RNATM_TRACE_RAY_ARGS_NULL__ {                                          \
  {0,0,0}, /* Ray origin */                                                    \
  {0,0,0}, /* Ray direction */                                                 \
  {0,DBL_MAX}, /* Ray range */                                                 \
                                                                               \
  NULL, /* Challenge functor */                                                \
  NULL, /* Filter functor */                                                   \
  NULL, /* User defined data */                                                \
                                                                               \
  SIZE_MAX, /* Index of the spectral band */                                   \
  SIZE_MAX  /* Index of the quadrature point */                                \
}
static const struct rnatm_trace_ray_args
RNATM_TRACE_RAY_ARGS_NULL = RNATM_TRACE_RAY_ARGS_NULL__;

struct rnatm_create_args {
  struct rnatm_gas_args gas;
  struct rnatm_aerosol_args* aerosols;
  size_t naerosols;
  char* name; /* Name of the atmosphere */

  /* Read/write file where octrees are offloaded. May be NULL => octrees are
   * built at runtime and kept in memory */
  FILE* octrees_storage;

  /* Defines whether the octrees to be taken into account have already been
   * unloaded into 'octrees_storage' at a previous run and, therefore, can be
   * loaded from the provided file rather than built from scratch */
  int load_octrees_from_storage;

  /* Spectral range to consider (in nanometers). Limits are inclusive */
  double spectral_range[2];
  double optical_thickness; /* Threshold used during octree building */

  unsigned grid_definition_hint; /* Hint on the grid definition */
  int precompute_normals; /* Pre-compute the tetrahedra normals */

  struct logger* logger; /* NULL <=> use default logger */
  struct mem_allocator* allocator; /* NULL <=> use default allocator */

  unsigned nthreads; /* Hint on the number of threads to use */
  int verbose; /* Verbosity level */
};
#define RNATM_CREATE_ARGS_DEFAULT__ {                                          \
  RNATM_GAS_ARGS_NULL__, /* Gas */                                             \
  NULL, /* Aerosols */                                                         \
  0, /* Number of aerosols */                                                  \
  "atmosphere", /* Name */                                                     \
                                                                               \
  NULL, /* octrees storage */                                                  \
  0,                                                                           \
                                                                               \
  {380, 780}, /* Spectral range */                                             \
  1, /* Optical thickness */                                                   \
                                                                               \
  512, /* Hint on the grid definition */                                       \
  0, /* Precompute tetrahedra normals */                                       \
                                                                               \
  NULL, /* Logger */                                                           \
  NULL, /* Allocator */                                                        \
                                                                               \
  (unsigned)~0, /* #threads */                                                 \
  0 /* Verbosity level */                                                      \
}
static const struct rnatm_create_args RNATM_CREATE_ARGS_DEFAULT =
  RNATM_CREATE_ARGS_DEFAULT__;

/* Opaque data types */
struct rnatm;

/* Helper macro that returns the cell of a component from a list of
 * rnatm_cell_pos returned by the rnatm_fetch_cell_list function */
#define RNATM_GET_COMPONENT_CELL(Cells, Cpnt) ((Cells)[(Cpnt)+1])

BEGIN_DECLS

/*******************************************************************************
 * API of the Rad-Net ATMosphere library
 ******************************************************************************/
RNATM_API res_T
rnatm_create
  (const struct rnatm_create_args* args,
   struct rnatm** atm);

RNATM_API res_T
rnatm_ref_get
  (struct rnatm* atm);

RNATM_API res_T
rnatm_ref_put
  (struct rnatm* atm);

/* Validates the atmosphere data. Data checks have already been done on load,
 * but this function performs longer tests: for example, it iterates over all
 * indices of the aerosol phase function check their validity against the mesh
 * they are associated with and the phase function list loaded */
RNATM_API res_T
rnatm_validate
  (const struct rnatm* atm);

RNATM_API double
rnatm_get_k_svx_voxel
  (const struct rnatm* atm,
   const struct svx_voxel* voxel,
   const enum rnatm_radcoef radcoef,
   const enum rnatm_svx_op op);

RNATM_API res_T
rnatm_get_radcoef
  (const struct rnatm* atm,
   const struct rnatm_get_radcoef_args* args,
   double* k);

RNATM_API res_T
rnatm_sample_component
  (const struct rnatm* atm,
   const struct rnatm_sample_component_args* args,
   size_t* cpnt);

RNATM_API res_T
rnatm_fetch_cell
  (const struct rnatm* atm,
   const double pos[3],
   /* Component. This is either an aerosol index or the RNATM_GAS constant */
   const size_t cpnt,
   struct rnatm_cell_pos* cell);

/* Returns the cells of each component corresponding to the given position */
RNATM_API res_T
rnatm_fetch_cell_list
  (const struct rnatm* atm,
   const double pos[3],
   /* The capacity of the submitted cell array must be greater than or equal to
    * the number of atmospheric components. If you are not sure, allocate (on
    * the stack or on the heap) an array whose capacity is
    * RNATM_MAX_COMPONENTS_COUNT */
   struct rnatm_cell_pos* cells,
   size_t* ncells); /* Total number of atmospheric components. May be NULL */

RNATM_API res_T
rnatm_cell_get_radcoef
  (const struct rnatm* atm,
   const struct rnatm_cell_get_radcoef_args* args,
   double* k);

RNATM_API res_T
rnatm_cell_create_phase_fn
  (struct rnatm* atm,
   const struct rnatm_cell_create_phase_fn_args* args,
   struct ssf_phase** phase_fn);

RNATM_API res_T
rnatm_cell_get_gas_temperature
  (const struct rnatm* atm,
   const struct rnatm_cell_pos* cell, /* Must belongs to the gas */
   double* temperature);

RNATM_API res_T
rnatm_trace_ray
  (struct rnatm* rnatm,
   const struct rnatm_trace_ray_args* args,
   struct svx_hit* hit);

RNATM_API size_t
rnatm_get_aerosols_count
  (const struct rnatm* atm);

/* Returns the number of spectral items. One acceleration structure is built
 * per spectral item from the gas and aerosols meshes */
RNATM_API size_t
rnatm_get_spectral_items_count
  (const struct rnatm* atm);

/* Returns the range of band indices covered by a given spectral range. The
 * returned index range is degenerated (i.e. ibands[0] > ibands[1]) if no band
 * is found */
RNATM_API res_T
rnatm_find_bands
  (const struct rnatm* rnatm,
   const double range[2], /* In nm. Limits are inclusive */
   size_t ibands[2]); /* Range of overlaped bands. Limits are inclusive */

RNATM_API res_T
rnatm_band_sample_quad_pt
  (const struct rnatm* rnatm,
   const double r, /* Canonical random number in [0, 1[ */
   const size_t iband, /* Index of the band to sample */
   size_t* iquad);

RNATM_API res_T
rnatm_band_get_desc
  (const struct rnatm* rnatm,
   const size_t iband, /* Index of the band to query */
   struct rnatm_band_desc* band);

/* Writes a set of octrees following the VTK file format */
RNATM_API res_T
rnatm_write_vtk_octrees
  (struct rnatm* atm,
   /* Spectral items to consider. Limits are inclusive. There is one octree per
    * spectral item */
   const size_t spectral_items[2],
   FILE* stream);

END_DECLS

#endif /* RNATM_H */
