/* Copyright (C) 2022, 2023 Centre National de la Recherche Scientifique
 * Copyright (C) 2022, 2023 Institut Pierre-Simon Laplace
 * Copyright (C) 2022, 2023 Institut de Physique du Globe de Paris
 * Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2022, 2023 Observatoire de Paris
 * Copyright (C) 2022, 2023 Université de Reims Champagne-Ardenne
 * Copyright (C) 2022, 2023 Université de Versaille Saint-Quentin
 * Copyright (C) 2022, 2023 Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef RNATM_C_H
#define RNATM_C_H

#include "rnatm.h"

#include <rsys/dynamic_array_size_t.h>
#include <rsys/logger.h>
#include <rsys/ref_count.h>
#include <rsys/str.h>

struct rnsf;
struct ssf_phase;

/* Generate the dynamic array of dynamic array of size_t */
#define DARRAY_NAME size_t_list
#define DARRAY_DATA struct darray_size_t
#define DARRAY_FUNCTOR_INIT darray_size_t_init
#define DARRAY_FUNCTOR_RELEASE darray_size_t_release
#define DARRAY_FUNCTOR_COPY darray_size_t_copy
#define DARRAY_FUNCTOR_COPY_AND_RELEASE darray_size_t_copy_and_release
#include <rsys/dynamic_array.h>

/*******************************************************************************
 * Phase function (SSF)
 ******************************************************************************/
extern LOCAL_SYM res_T
phase_init
  (struct mem_allocator* allocator,
   struct ssf_phase** phase);

extern LOCAL_SYM void
phase_release
  (struct ssf_phase** phase);

extern LOCAL_SYM res_T
phase_copy
  (struct ssf_phase** dst,
   struct ssf_phase* const* src);

extern LOCAL_SYM res_T
phase_copy_and_release
  (struct ssf_phase** dst,
   struct ssf_phase** src);

/* Generate the dynamic array of SSF phase function */
#define DARRAY_NAME phase
#define DARRAY_DATA struct ssf_phase*
#define DARRAY_FUNCTOR_INIT phase_init
#define DARRAY_FUNCTOR_RELEASE phase_release
#define DARRAY_FUNCTOR_COPY phase_copy
#define DARRAY_FUNCTOR_COPY_AND_RELEASE phase_copy_and_release
#include <rsys/dynamic_array.h>

/*******************************************************************************
 * Phase function
 ******************************************************************************/
struct phase_fn {
  struct rnsf* rnsf;
  struct darray_phase phase_lst;
};

extern LOCAL_SYM res_T
phase_fn_init
  (struct mem_allocator* allocator,
   struct phase_fn* phase_fn);

extern LOCAL_SYM void
phase_fn_release
  (struct phase_fn* phase_fn);

extern LOCAL_SYM res_T
phase_fn_copy
  (struct phase_fn* dst,
   const struct phase_fn* src);

extern LOCAL_SYM res_T
phase_fn_copy_and_release
  (struct phase_fn* dst,
   struct phase_fn* src);

/* Generate the dynamic array of RNSF phase functions */
#define DARRAY_NAME phase_fn
#define DARRAY_DATA struct phase_fn
#define DARRAY_FUNCTOR_INIT phase_fn_init
#define DARRAY_FUNCTOR_RELEASE phase_fn_release
#define DARRAY_FUNCTOR_COPY phase_fn_copy
#define DARRAY_FUNCTOR_COPY_AND_RELEASE phase_fn_copy_and_release
#include <rsys/dynamic_array.h>

/*******************************************************************************
 * Gas
 ******************************************************************************/
struct gas {
  struct suvm_volume* volume;
  struct ssf_phase* rayleigh;
  struct sbuf* temperatures;
  struct sck* ck;
  size_t ntetrahedra;
  size_t nvertices;
};

extern LOCAL_SYM res_T
gas_init
  (struct mem_allocator* allocator,
   struct gas* gas);

extern LOCAL_SYM void
gas_release
  (struct gas* gas);

extern LOCAL_SYM res_T
gas_copy
  (struct gas* dst,
   const struct gas* src);

extern LOCAL_SYM res_T
gas_copy_and_release
  (struct gas* dst,
   struct gas* src);

/*******************************************************************************
 * Aerosol
 ******************************************************************************/
struct aerosol {
  struct darray_phase_fn phase_fn_lst;
  struct str name;
  struct suvm_volume* volume;
  struct sbuf* phase_fn_ids;
  struct sars* sars;
  size_t ntetrahedra;
  size_t nvertices;
};

extern LOCAL_SYM res_T
aerosol_init
  (struct mem_allocator* allocator,
   struct aerosol* aerosol);

extern LOCAL_SYM void
aerosol_release
  (struct aerosol* aerosol);

extern LOCAL_SYM res_T
aerosol_copy
  (struct aerosol* dst,
   const struct aerosol* src);

extern LOCAL_SYM res_T
aerosol_copy_and_release
  (struct aerosol* dst,
   struct aerosol* src);

/* Define the dynamic array of aerosols */
#define DARRAY_NAME aerosol
#define DARRAY_DATA struct aerosol
#define DARRAY_FUNCTOR_INIT aerosol_init
#define DARRAY_FUNCTOR_RELEASE aerosol_release
#define DARRAY_FUNCTOR_COPY aerosol_copy
#define DARRAY_FUNCTOR_COPY_AND_RELEASE aerosol_copy_and_release
#include <rsys/dynamic_array.h>

/*******************************************************************************
 * Acceleration structure
 ******************************************************************************/
struct accel_struct {
  struct svx_tree* octree;
  fpos_t fpos; /* Position of the serialized octree data in the storage file */
  size_t iband; /* Index of the spectral band */
  size_t iquad_pt; /* Index of the quadrature point */
};

extern LOCAL_SYM res_T
accel_struct_init
  (struct mem_allocator* allocator,
   struct accel_struct* accel_struct);

extern LOCAL_SYM void
accel_struct_release
  (struct accel_struct* accel_struct);

extern LOCAL_SYM res_T
accel_struct_copy
  (struct accel_struct* dst,
   const struct accel_struct* src);

extern LOCAL_SYM res_T
accel_struct_copy_and_release
  (struct accel_struct* dst,
   struct accel_struct* src);

/* Define the dynamic array of acceleration structures */
#define DARRAY_NAME accel_struct
#define DARRAY_DATA struct accel_struct
#define DARRAY_FUNCTOR_INIT accel_struct_init
#define DARRAY_FUNCTOR_RELEASE accel_struct_release
#define DARRAY_FUNCTOR_COPY accel_struct_copy
#define DARRAY_FUNCTOR_COPY_AND_RELEASE accel_struct_copy_and_release
#include <rsys/dynamic_array.h>

/*******************************************************************************
 * Spectral band
 ******************************************************************************/
struct band {
  size_t index; /* Index of the spectral band */
  size_t nquad_pts; /* Number of quadrature points in the band */

  /* Offset toward the first acceleration structure of the band */
  size_t offset_accel_struct;
};

/* Define the dynamic array of spectral bands */
#define DARRAY_NAME band
#define DARRAY_DATA struct band
#include <rsys/dynamic_array.h>

/*******************************************************************************
 * Atmosphere
 ******************************************************************************/
struct rnatm {
  struct gas gas;
  struct darray_aerosol aerosols;
  struct darray_accel_struct accel_structs;
  struct darray_band bands; /* Spectral bands to be taken into account */
  struct str name;

  FILE* octrees_storage;

  unsigned grid_definition[3];

  /* Spectral range to consider (in nanometers). Limits are inclusive */
  double spectral_range[2];
  /* Ids of the bands covered by the spectral range. Limits are inclusive */
  size_t ibands[2];
  double optical_thickness; /* Threshold used during octree building */

  unsigned nthreads;
  int verbose;

  struct logger* logger;
  struct logger logger__;
  struct mem_allocator* allocator;
  struct mem_allocator svx_allocator;

  struct mutex* mutex;

  struct svx_device* svx;
  int svx_allocator_is_init;
  ref_T ref;
};

extern LOCAL_SYM res_T
setup_meshes
  (struct rnatm* atm,
   const struct rnatm_create_args* args);

extern LOCAL_SYM res_T
setup_properties
  (struct rnatm* atm,
   const struct rnatm_create_args* args);

extern LOCAL_SYM res_T
check_properties
  (const struct rnatm* atm);

extern LOCAL_SYM res_T
setup_octrees
  (struct rnatm* atm,
   const struct rnatm_create_args* args);

extern LOCAL_SYM res_T
make_sure_octree_is_loaded
  (struct rnatm* atm,
   const size_t iaccel_struct);

extern LOCAL_SYM void
unload_octree
  (struct rnatm* atm,
   const size_t iaccel_struct);

/* Get the radiative coefficient of the input tetrahedron. The output radiative
 * coefficients per vertex are defined even if the tetrahedron is invalid or if
 * no spectral data is defined for this component for the considered band. In
 * such cases, the k returned are zero and the function returns 0 instead of 1*/
extern LOCAL_SYM int
tetra_get_radcoef
  (const struct rnatm* atm,
   const struct suvm_primitive* tetra,
   const size_t component,
   const size_t iband,
   const size_t iquad_pt,
   const enum rnatm_radcoef radcoef,
   float k[4]);

#endif /* RNATM_C_H */
