/* Copyright (C) 2022, 2023 Centre National de la Recherche Scientifique
 * Copyright (C) 2022, 2023 Institut Pierre-Simon Laplace
 * Copyright (C) 2022, 2023 Institut de Physique du Globe de Paris
 * Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2022, 2023 Observatoire de Paris
 * Copyright (C) 2022, 2023 Université de Reims Champagne-Ardenne
 * Copyright (C) 2022, 2023 Université de Versaille Saint-Quentin
 * Copyright (C) 2022, 2023 Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "rnatm.h"
#include "rnatm_c.h"
#include "rnatm_log.h"
#include "rnatm_voxel.h"

#include <rad-net/rnsf.h>

#include <star/sars.h>
#include <star/sbuf.h>
#include <star/sck.h>
#include <star/ssf.h>
#include <star/suvm.h>
#include <star/svx.h>

#include <rsys/cstr.h>
#include <rsys/mem_allocator.h>
#include <rsys/mutex.h>

#include <omp.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static INLINE res_T
check_rnatm_gas_args(const struct rnatm_gas_args* args)
{
  if(!args) return RES_BAD_ARG;

  /* Filenames cannot be NULL */
  if(!args->smsh_filename
  || !args->sck_filename
  || !args->temperatures_filename)
    return RES_BAD_ARG;

  return RES_OK;
}

static INLINE res_T
check_rnatm_aerosol_args(const struct rnatm_aerosol_args* args)
{
  if(!args) return RES_BAD_ARG;

  /* Filenames cannot be NULL */
  if(!args->smsh_filename
  || !args->sars_filename
  || !args->phase_fn_ids_filename
  || !args->phase_fn_lst_filename)
    return RES_BAD_ARG;

  return RES_OK;
}

static res_T
check_rnatm_create_args(const struct rnatm_create_args* args)
{
  size_t i;
  res_T res = RES_OK;

  /* Invalid args */
  if(!args) return RES_BAD_ARG;

  /* Invalid total number of components */
  if(args->naerosols + 1/*gas*/ >= RNATM_MAX_COMPONENTS_COUNT)
    return RES_BAD_ARG;

  /* Invalid gas */
  res = check_rnatm_gas_args(&args->gas);
  if(res != RES_OK) return res;

  /* Invalid aerosols */
  FOR_EACH(i, 0, args->naerosols) {
    res = check_rnatm_aerosol_args(args->aerosols+i);
    if(res != RES_OK) return res;
  }

  /* Invalid spectral range */
  if(args->spectral_range[0] > args->spectral_range[1])
    return RES_BAD_ARG;

  /* Invalid requirements for loading octrees */
  if(!args->octrees_storage && args->load_octrees_from_storage)
    return RES_BAD_ARG;

  /* Check miscalleneous arguments */
  if(!args->name
  || args->optical_thickness < 0
  || !args->grid_definition_hint
  || !args->nthreads)
    return RES_BAD_ARG;

  return RES_OK;
}

static res_T
setup_aerosol_name
  (struct rnatm* atm,
   const struct rnatm_create_args* args,
   const size_t iaerosol)
{
  struct aerosol* aerosol = NULL;
  res_T res = RES_OK;
  ASSERT(atm && args);
  ASSERT(iaerosol < args->naerosols);
  ASSERT(args->naerosols == darray_aerosol_size_get(&atm->aerosols));

  aerosol = darray_aerosol_data_get(&atm->aerosols)+iaerosol;

  /* Use user-defined name */
  if(args->aerosols[iaerosol].name) {
    res = str_set(&aerosol->name, args->aerosols[iaerosol].name);
    if(res != RES_OK) {
      log_err(atm, "could not set the name of the aerosol %lu to `%s' -- %s\n",
        (unsigned long)iaerosol,
        args->aerosols[iaerosol].name,
        res_to_cstr(res));
      goto error;
    }

  /* Use default name */
  } else {
    res = str_printf(&aerosol->name, "aerosol%lu", (unsigned long)iaerosol);
    if(res != RES_OK) {
      log_err(atm,
        "could not set the name of the aerosol %lu to `aerosol%lu' -- %s\n",
        (unsigned long)iaerosol,
        (unsigned long)iaerosol,
        res_to_cstr(res));
      goto error;
    }
  }

exit:
  return res;
error:
  goto exit;
}

static res_T
create_rnatm
  (const struct rnatm_create_args* args,
   struct rnatm** out_atm)
{
  struct rnatm* atm = NULL;
  struct mem_allocator* allocator = NULL;
  size_t i = 0;
  int nthreads_max = 0;
  res_T res = RES_OK;

  if(!out_atm) { res = RES_BAD_ARG; goto error;}
  res = check_rnatm_create_args(args);
  if(res != RES_OK) goto error;

  allocator = args->allocator ? args->allocator : &mem_default_allocator;
  atm = MEM_CALLOC(allocator, 1, sizeof(*atm));
  if(!atm) {
    if(args->verbose) {
      #define ERR_STR \
        "could not allocate the device of the Rad-Net ATMosphere library"
      if(args->logger) {
        logger_print(args->logger, LOG_ERROR, ERR_STR);
      } else {
        fprintf(stderr, MSG_ERROR_PREFIX ERR_STR);
      }
      #undef ERR_STR
    }
    res = RES_MEM_ERR;
    goto error;
  }
  ref_init(&atm->ref);
  atm->allocator = allocator;
  atm->verbose = args->verbose;
  str_init(atm->allocator, &atm->name);
  gas_init(atm->allocator, &atm->gas);
  darray_aerosol_init(atm->allocator, &atm->aerosols);
  darray_accel_struct_init(atm->allocator, &atm->accel_structs);
  darray_band_init(atm->allocator, &atm->bands);

  /* Setup the number of threads */
  nthreads_max = MMAX(omp_get_max_threads(), omp_get_num_procs());
  atm->nthreads = MMIN((unsigned)nthreads_max, args->nthreads);

  if(args->logger) {
    atm->logger = args->logger;
  } else {
    res = setup_log_default(atm);
    if(res != RES_OK) {
      if(args->verbose) {
        fprintf(stderr, MSG_ERROR_PREFIX
          "could not setup the default logger of the "
          "Rad-Net ATMopshere library\n");
      }
      goto error;
    }
  }

  res = darray_aerosol_resize(&atm->aerosols, args->naerosols);
  if(res != RES_OK) {
    log_err(atm, "could not allocate aerosol list -- %s\n", res_to_cstr(res));
    goto error;
  }

  res = str_set(&atm->name, args->name);
  if(res != RES_OK) {
    log_err(atm, "could not setup the atmosphere name to `%s' -- %s\n",
      args->name, res_to_cstr(res));
    goto error;
  }

  FOR_EACH(i, 0, args->naerosols) {
    res = setup_aerosol_name(atm, args, i);
    if(res != RES_OK) goto error;
  }

  res = mem_init_regular_allocator(&atm->svx_allocator);
  if(res != RES_OK) {
    log_err(atm,
      "unable to initialize the allocator used to manage the memory of the "
      "Star-VoXel library -- %s\n", res_to_cstr(res));
    goto error;
  }
  atm->svx_allocator_is_init = 1;

  atm->mutex = mutex_create();
  if(!atm->mutex) {
    log_err(atm, "unable to create Rad-Net ATMopshere library mutex\n");
    res = RES_MEM_ERR;
    goto error;
  }

exit:
  if(out_atm) *out_atm = atm;
  return res;
error:
  if(atm) { RNATM(ref_put(atm)); atm = NULL; }
  goto exit;
}

static void
release_rnatm(ref_T* ref)
{
  struct rnatm* atm = CONTAINER_OF(ref, struct rnatm, ref);
  ASSERT(ref);
  if(atm->logger == &atm->logger__) logger_release(&atm->logger__);
  if(atm->svx) SVX(device_ref_put(atm->svx));
  if(atm->mutex) mutex_destroy(atm->mutex);
  darray_aerosol_release(&atm->aerosols);
  darray_accel_struct_release(&atm->accel_structs);
  darray_band_release(&atm->bands);
  if(atm->svx_allocator_is_init) {
    ASSERT(MEM_ALLOCATED_SIZE(&atm->svx_allocator) == 0);
    mem_shutdown_regular_allocator(&atm->svx_allocator);
  }
  gas_release(&atm->gas);
  str_release(&atm->name);
  MEM_RM(atm->allocator, atm);
}

/*******************************************************************************
 * Exported symbols
 ******************************************************************************/
res_T
rnatm_create
  (const struct rnatm_create_args* args,
   struct rnatm** out_atm)
{
  struct rnatm* atm = NULL;
  res_T res = RES_OK;

  res = create_rnatm(args, &atm);
  if(res != RES_OK) goto error;

  res = setup_meshes(atm, args);
  if(res != RES_OK) goto error;
  res = setup_properties(atm, args);
  if(res != RES_OK) goto error;
  res = setup_octrees(atm, args);
  if(res != RES_OK) goto error;

exit:
  if(out_atm) *out_atm = atm;
  return res;
error:
  if(atm) { RNATM(ref_put(atm)); atm = NULL; }
  goto exit;
}

res_T
rnatm_ref_get(struct rnatm* atm)
{
  if(!atm) return RES_BAD_ARG;
  ref_get(&atm->ref);
  return RES_OK;
}

res_T
rnatm_ref_put(struct rnatm* atm)
{
  if(!atm) return RES_BAD_ARG;
  ref_put(&atm->ref, release_rnatm);
  return RES_OK;
}

res_T
rnatm_validate(const struct rnatm* atm)
{
  res_T res = RES_OK;

  if(!atm) { res = RES_BAD_ARG; goto error; }

  res = check_properties(atm);
  if(res != RES_OK) goto error;

exit:
  return res;
error:
  goto exit;
}

double
rnatm_get_k_svx_voxel
  (const struct rnatm* atm,
   const struct svx_voxel* voxel,
   const enum rnatm_radcoef radcoef,
   const enum rnatm_svx_op op)
{
  const float* vx;
  ASSERT(atm && voxel);
  ASSERT((unsigned)radcoef < RNATM_RADCOEFS_COUNT__);
  ASSERT((unsigned)op < RNATM_SVX_OPS_COUNT__);
  (void)atm;

  vx = voxel->data;

  /* Absorption/Scattering coefficient */
  if(radcoef != RNATM_RADCOEF_Kext) {
    const float k_min = vx[voxel_idata(radcoef, RNATM_SVX_OP_MIN)];
    const float k_max = vx[voxel_idata(radcoef, RNATM_SVX_OP_MAX)];
    if(k_min > k_max) return 0; /* Empty voxel => null radiative coefficient */
    switch(op) {
      case RNATM_SVX_OP_MIN: return k_min;
      case RNATM_SVX_OP_MAX: return k_max;
      default: FATAL("Unreachable code\n"); break;
    }

  /* Extinction coefficient */
  } else {
    const float ka_min = vx[voxel_idata(RNATM_RADCOEF_Ka, RNATM_SVX_OP_MIN)];
    const float ka_max = vx[voxel_idata(RNATM_RADCOEF_Ka, RNATM_SVX_OP_MAX)];
    const float ks_min = vx[voxel_idata(RNATM_RADCOEF_Ks, RNATM_SVX_OP_MIN)];
    const float ks_max = vx[voxel_idata(RNATM_RADCOEF_Ks, RNATM_SVX_OP_MAX)];

    /* Unlike absorption and diffusion coefficients, the range of the
     * extinction coefficient is not stored by voxel but is recalculated from
     * ka and ks by voxel; k_ext = ka + ks. However, the extinction coefficient
     * of a tetrahedron is calculated by first adding ka and ks per component
     * before adding them to obtain the k_ext of the mixture. Although the
     * results should be identical, the reorganization of sums produces a
     * numerical inconsistency. Therefore, the k_ext tetrahedron might not be
     * strictly included in the range of k_ext computed for the voxel. To deal
     * with this numerical inaccuracy, we therefore use the following epsilon
     * to slightly increase the returned range of k_ext */
    const float epsilon = 1e-6f;

    /* Empty voxel => null radiative coefficient */
    if(ka_min > ka_max) { ASSERT(ks_min > ks_max); return 0; }

    switch(op) {
      case RNATM_SVX_OP_MIN: return (ka_min + ks_min)*(1.f-epsilon);
      case RNATM_SVX_OP_MAX: return (ka_max + ks_max)*(1.f+epsilon);
      default: FATAL("Unreachable code\n"); break;
    }
  }
}

size_t
rnatm_get_aerosols_count(const struct rnatm* atm)
{
  ASSERT(atm);
  return darray_aerosol_size_get(&atm->aerosols);
}

size_t
rnatm_get_spectral_items_count(const struct rnatm* atm)
{
  ASSERT(atm);
  return darray_accel_struct_size_get(&atm->accel_structs);
}

res_T
rnatm_find_bands
  (const struct rnatm* rnatm,
   const double range[2], /* In nm. Limits are inclusive */
   size_t ibands[2]) /* Range of overlaped bands. Limits are inclusive */
{
  double range_adjusted[2];

  if(!rnatm
  || !range
  || range[0] > rnatm->spectral_range[1]
  || range[1] < rnatm->spectral_range[0])
    return RES_BAD_ARG;

  /* Clamp the submitted range to the spectral domain of the atmosphere */
  range_adjusted[0] = MMAX(range[0], rnatm->spectral_range[0]);
  range_adjusted[1] = MMIN(range[1], rnatm->spectral_range[1]);

  return sck_find_bands(rnatm->gas.ck, range_adjusted, ibands);
}

res_T
rnatm_band_sample_quad_pt
  (const struct rnatm* rnatm,
   const double r, /* Canonical random number in [0, 1[ */
   const size_t iband,
   size_t* iquad)
{
  struct sck_band band;
  res_T res = RES_OK;

  if(!rnatm || !iquad) {
    res = RES_BAD_ARG;
    goto error;
  }

  res = sck_get_band(rnatm->gas.ck, iband, &band);
  if(res != RES_OK) goto error;

  /* Reject the band if it does not overlap the atmosphere spectral domain */
  if(band.lower/*Inclusive*/  > rnatm->spectral_range[1]
  || band.upper/*Exclusive*/ <= rnatm->spectral_range[0]) {
    return RES_BAD_ARG;
  }

  res = sck_band_sample_quad_pt(&band, r, iquad);
  if(res != RES_OK) goto error;

exit:
  return res;
error:
  goto exit;
}

res_T
rnatm_band_get_desc
  (const struct rnatm* rnatm,
   const size_t iband,
   struct rnatm_band_desc* band_desc)
{
  struct sck_band band;
  res_T res = RES_OK;

  if(!rnatm || !band_desc) {
    res = RES_BAD_ARG;
    goto error;
  }

  res = sck_get_band(rnatm->gas.ck, iband, &band);
  if(res != RES_OK) goto error;

  /* Reject the band if it does not overlap the atmosphere spectral domain */
  if(band.lower/*Inclusive*/  > rnatm->spectral_range[1]
  || band.upper/*Exclusive*/ <= rnatm->spectral_range[0]) {
    return RES_BAD_ARG;
  }

  /* TODO Should the range of the band be clamped to the atmospheric spectral
   * domain? */
  band_desc->lower = band.lower;
  band_desc->upper = band.upper;
  band_desc->quad_pts_count = band.quad_pts_count;

exit:
  return res;
error:
  goto exit;
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
res_T
phase_init(struct mem_allocator* allocator, struct ssf_phase** phase)
{
  ASSERT(phase);
  (void)allocator;
  *phase = NULL;
  return RES_OK;
}

void
phase_release(struct ssf_phase** phase)
{
  ASSERT(phase);
  if(*phase) SSF(phase_ref_put(*phase));
}

res_T
phase_copy(struct ssf_phase** dst, struct ssf_phase* const* src)
{
  ASSERT(dst && src);
  if(*dst) SSF(phase_ref_put(*dst));
  *dst = *src;
  if(*dst) SSF(phase_ref_get(*dst));
  return RES_OK;
}

res_T
phase_copy_and_release(struct ssf_phase** dst, struct ssf_phase** src)
{
  ASSERT(dst && src);
  if(*dst) SSF(phase_ref_put(*dst));
  *dst = *src;
  *src = NULL;
  return RES_OK;
}

res_T
phase_fn_init(struct mem_allocator* allocator, struct phase_fn* phase_fn)
{
  ASSERT(phase_fn);
  phase_fn->rnsf = NULL;
  darray_phase_init(allocator, &phase_fn->phase_lst);
  return RES_OK;
}

void
phase_fn_release(struct phase_fn* phase_fn)
{
  ASSERT(phase_fn);
  if(phase_fn->rnsf) RNSF(ref_put(phase_fn->rnsf));
  darray_phase_release(&phase_fn->phase_lst);
}

res_T
phase_fn_copy(struct phase_fn* dst, const struct phase_fn* src)
{
  ASSERT(dst && src);
  if(dst->rnsf) RNSF(ref_put(dst->rnsf));
  dst->rnsf = src->rnsf;
  if(dst->rnsf) RNSF(ref_get(dst->rnsf));
  return darray_phase_copy(&dst->phase_lst, &src->phase_lst);
}

res_T
phase_fn_copy_and_release(struct phase_fn* dst, struct phase_fn* src)
{
  ASSERT(dst && src);
  if(dst->rnsf) RNSF(ref_put(dst->rnsf));
  dst->rnsf = src->rnsf;
  src->rnsf = NULL;
  return darray_phase_copy_and_release(&dst->phase_lst, &src->phase_lst);
}

res_T
gas_init(struct mem_allocator* allocator, struct gas* gas)
{
  (void)allocator;
  ASSERT(gas);
  gas->volume = NULL;
  gas->rayleigh = NULL;
  gas->temperatures = NULL;
  gas->ck = NULL;
  gas->ntetrahedra = 0;
  gas->nvertices = 0;
  return RES_OK;
}

void
gas_release(struct gas* gas)
{
  ASSERT(gas);
  if(gas->volume) SUVM(volume_ref_put(gas->volume));
  if(gas->rayleigh) SSF(phase_ref_put(gas->rayleigh));
  if(gas->temperatures) SBUF(ref_put(gas->temperatures));
  if(gas->ck) SCK(ref_put(gas->ck));
}

res_T
gas_copy(struct gas* dst, const struct gas* src)
{
  ASSERT(dst && src);
  if(dst->volume) SUVM(volume_ref_put(dst->volume));
  if(dst->rayleigh) SSF(phase_ref_put(dst->rayleigh));
  if(dst->temperatures) SBUF(ref_put(dst->temperatures));
  if(dst->ck) SCK(ref_put(dst->ck));
  dst->volume = src->volume;
  dst->rayleigh = src->rayleigh;
  dst->temperatures = src->temperatures;
  dst->ck = src->ck;
  dst->ntetrahedra = src->ntetrahedra;
  dst->nvertices = src->nvertices;
  if(dst->volume) SUVM(volume_ref_get(dst->volume));
  if(dst->rayleigh) SSF(phase_ref_get(dst->rayleigh));
  if(dst->temperatures) SBUF(ref_get(dst->temperatures));
  if(dst->ck) SCK(ref_get(dst->ck));
  return RES_OK;
}

res_T
gas_copy_and_release(struct gas* dst, struct gas* src)
{
  ASSERT(dst && src);
  if(dst->volume) SUVM(volume_ref_put(dst->volume));
  if(dst->rayleigh) SSF(phase_ref_put(dst->rayleigh));
  if(dst->temperatures) SBUF(ref_put(dst->temperatures));
  if(dst->ck) SCK(ref_put(dst->ck));
  dst->volume = src->volume;
  dst->rayleigh = src->rayleigh;
  dst->temperatures = src->temperatures;
  dst->ck = src->ck;
  dst->ntetrahedra = src->ntetrahedra;
  dst->nvertices = src->nvertices;
  src->volume = NULL;
  src->rayleigh = NULL;
  src->temperatures = NULL;
  src->ck = NULL;
  return RES_OK;
}

res_T
aerosol_init(struct mem_allocator* allocator, struct aerosol* aerosol)
{
  (void)allocator;
  ASSERT(aerosol);
  darray_phase_fn_init(allocator, &aerosol->phase_fn_lst);
  str_init(allocator, &aerosol->name);
  aerosol->volume = NULL;
  aerosol->phase_fn_ids = NULL;
  aerosol->sars = NULL;
  aerosol->ntetrahedra = 0;
  aerosol->nvertices = 0;
  return RES_OK;
}

void
aerosol_release(struct aerosol* aerosol)
{
  ASSERT(aerosol);
  darray_phase_fn_release(&aerosol->phase_fn_lst);
  str_release(&aerosol->name);
  if(aerosol->volume) SUVM(volume_ref_put(aerosol->volume));
  if(aerosol->phase_fn_ids) SBUF(ref_put(aerosol->phase_fn_ids));
  if(aerosol->sars) SARS(ref_put(aerosol->sars));
}

res_T
aerosol_copy(struct aerosol* dst, const struct aerosol* src)
{
  res_T res = RES_OK;
  ASSERT(dst && src);

  if(dst->volume) SUVM(volume_ref_put(dst->volume));
  if(dst->phase_fn_ids) SBUF(ref_put(dst->phase_fn_ids));
  if(dst->sars) SARS(ref_put(dst->sars));

  dst->volume = src->volume;
  dst->phase_fn_ids = src->phase_fn_ids;
  dst->sars = src->sars;
  dst->ntetrahedra = src->ntetrahedra;
  dst->nvertices = src->nvertices;

  if(dst->volume) SUVM(volume_ref_get(dst->volume));
  if(dst->phase_fn_ids) SBUF(ref_get(dst->phase_fn_ids));
  if(dst->sars) SARS(ref_get(dst->sars));

  res = darray_phase_fn_copy(&dst->phase_fn_lst, &src->phase_fn_lst);
  if(res != RES_OK) return res;
  res = str_copy(&dst->name, &src->name);
  if(res != RES_OK) return res;

  return RES_OK;
}

res_T
aerosol_copy_and_release(struct aerosol* dst, struct aerosol* src)
{
  res_T res = RES_OK;
  ASSERT(dst && src);

  if(dst->volume) SUVM(volume_ref_put(dst->volume));
  if(dst->phase_fn_ids) SBUF(ref_put(dst->phase_fn_ids));
  if(dst->sars) SARS(ref_put(dst->sars));
  dst->volume = src->volume;
  dst->phase_fn_ids = src->phase_fn_ids;
  dst->sars = src->sars;
  dst->ntetrahedra = src->ntetrahedra;
  dst->nvertices = src->nvertices;
  src->volume = NULL;
  src->phase_fn_ids = NULL;
  src->sars = NULL;

  res = darray_phase_fn_copy_and_release(&dst->phase_fn_lst, &src->phase_fn_lst);
  if(res != RES_OK) return res;
  res = str_copy_and_release(&dst->name, &src->name);
  if(res != RES_OK) return res;

  return RES_OK;
}

res_T
accel_struct_init
  (struct mem_allocator* allocator,
   struct accel_struct* accel_struct)
{
  (void)allocator;
  ASSERT(accel_struct);
  accel_struct->octree = NULL;
  accel_struct->iband = 0;
  accel_struct->iquad_pt = 0;
  memset(&accel_struct->fpos, 0, sizeof(accel_struct->fpos));
  return RES_OK;
}

void
accel_struct_release(struct accel_struct* accel_struct)
{
  ASSERT(accel_struct);
  if(accel_struct->octree) SVX(tree_ref_put(accel_struct->octree));
}

res_T
accel_struct_copy(struct accel_struct* dst, const struct accel_struct* src)
{
  ASSERT(dst && src);
  if(dst->octree) SVX(tree_ref_put(dst->octree));
  dst->octree = src->octree;
  dst->fpos = src->fpos;
  dst->iband = src->iband;
  dst->iquad_pt = src->iquad_pt;
  if(dst->octree) SVX(tree_ref_get(dst->octree));
  return RES_OK;
}

res_T
accel_struct_copy_and_release(struct accel_struct* dst, struct accel_struct* src)
{
  ASSERT(dst && src);
  if(dst->octree) SVX(tree_ref_put(dst->octree));
  dst->octree = src->octree;
  dst->fpos = src->fpos;
  dst->iband = src->iband;
  dst->iquad_pt = src->iquad_pt;
  src->octree = NULL;
  return RES_OK;
}

res_T
make_sure_octree_is_loaded(struct rnatm* atm, const size_t istruct)
{
  struct accel_struct* accel_struct = NULL;
  int err = 0;
  res_T res = RES_OK;
  ASSERT(atm && istruct < darray_accel_struct_size_get(&atm->accel_structs));

  accel_struct = darray_accel_struct_data_get(&atm->accel_structs) + istruct;

  mutex_lock(atm->mutex);

  if(accel_struct->octree) {
    mutex_unlock(atm->mutex);
    goto exit; /* The octree is already loaded */
  }

  /* Prepare to read the octree's data */
  err = fsetpos(atm->octrees_storage, &accel_struct->fpos);
  if(err != 0) {
    res = RES_IO_ERR;
    mutex_unlock(atm->mutex);
    goto error;
  }

  /* Deserialize the octree */
  res = svx_tree_create_from_stream
    (atm->svx, atm->octrees_storage, &accel_struct->octree);
  if(res != RES_OK) {
    mutex_unlock(atm->mutex);
    goto error;
  }

  mutex_unlock(atm->mutex);

exit:
  return res;
error:
  log_err(atm, "error loading octree %lu -- %s",
    (unsigned long)istruct, res_to_cstr(res));
  goto exit;
}

void
unload_octree(struct rnatm* atm, const size_t istruct)
{
  struct accel_struct* accel_struct = NULL;
  ASSERT(atm);
  ASSERT(istruct < darray_accel_struct_size_get(&atm->accel_structs));

  accel_struct = darray_accel_struct_data_get(&atm->accel_structs) + istruct;
  if(accel_struct->octree) {
    SVX(tree_ref_put(accel_struct->octree));
    accel_struct->octree = NULL;
  }
}
