/* Copyright (C) 2022, 2023 Centre National de la Recherche Scientifique
 * Copyright (C) 2022, 2023 Institut Pierre-Simon Laplace
 * Copyright (C) 2022, 2023 Institut de Physique du Globe de Paris
 * Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2022, 2023 Observatoire de Paris
 * Copyright (C) 2022, 2023 Université de Reims Champagne-Ardenne
 * Copyright (C) 2022, 2023 Université de Versaille Saint-Quentin
 * Copyright (C) 2022, 2023 Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L /* nextafter */

#include "rnatm_c.h"
#include "rnatm_log.h"
#include "rnatm_octrees_storage.h"

#include <star/sars.h>
#include <star/sck.h>
#include <star/suvm.h>
#include <star/svx.h>

#include <rsys/cstr.h>

#include <math.h> /* nextafter */

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static int
cmp_hash256(const void* a, const void* b)
{
  const char* hash0 = *(const hash256_T*)a;
  const char* hash1 = *(const hash256_T*)b;
  size_t i;
  FOR_EACH(i, 0, sizeof(hash256_T)) {
    if(hash0[i] < hash1[i]) {
      return -1;
    } else if(hash0[i] > hash1[i]) {
      return +1;
    }
  }
  return 0;
}

static INLINE res_T
compute_gas_mesh_signature(const struct gas* gas, hash256_T hash)
{
  struct suvm_mesh_desc mesh;
  res_T res = RES_OK;
  ASSERT(gas && hash);

  res = suvm_volume_get_mesh_desc(gas->volume, &mesh);
  if(res != RES_OK) goto error;
  res = suvm_mesh_desc_compute_hash(&mesh, hash);
  if(res != RES_OK) goto error;

exit:
  return res;
error:
  goto exit;
}

static INLINE res_T
compute_aerosol_mesh_signature(const struct aerosol* aerosol, hash256_T hash)
{
  struct suvm_mesh_desc mesh;
  res_T res = RES_OK;
  ASSERT(aerosol &&hash);

  res = suvm_volume_get_mesh_desc(aerosol->volume, &mesh);
  if(res != RES_OK) goto error;
  res = suvm_mesh_desc_compute_hash(&mesh, hash);
  if(res != RES_OK) goto error;

exit:
  return res;
error:
  goto exit;
}

static res_T
compute_mesh_signature(const struct rnatm* atm, hash256_T mesh_signature)
{
  hash256_T gas_signature;
  hash256_T* aerosol_signatures = NULL;
  size_t naerosols;
  size_t i;
  res_T res = RES_OK;
  ASSERT(atm && mesh_signature);

  /* Compute the mesh signature of the gas */
  res = compute_gas_mesh_signature(&atm->gas, gas_signature);
  if(res != RES_OK) goto error;

  naerosols = darray_aerosol_size_get(&atm->aerosols);

  if(!naerosols) {
    memcpy(mesh_signature, gas_signature, sizeof(hash256_T));
  } else {
    struct sha256_ctx ctx;

    aerosol_signatures = MEM_CALLOC(atm->allocator, naerosols, sizeof(hash256_T));
    if(!aerosol_signatures) { res = RES_MEM_ERR; goto error; }

    FOR_EACH(i, 0, naerosols) {
      const struct aerosol* aerosol = darray_aerosol_cdata_get(&atm->aerosols)+i;

      res = compute_aerosol_mesh_signature(aerosol, aerosol_signatures[i]);
      if(res != RES_OK) goto error;
    }

    /* Pay attention to the order of how aerosol hashes are registered.
     * Therefore, the same set of aerosols will have the same signature (i.e. the
     * same hash), regardless of the order in which the aerosols were listed in
     * the input arguments */
    qsort(aerosol_signatures, naerosols, sizeof(hash256_T), cmp_hash256);

    /* Build the signature for the dataset */
    sha256_ctx_init(&ctx);
    sha256_ctx_update(&ctx, gas_signature, sizeof(hash256_T));
    FOR_EACH(i, 0, naerosols) {
      sha256_ctx_update(&ctx, aerosol_signatures[i], sizeof(hash256_T));
    }
    sha256_ctx_finalize(&ctx, mesh_signature);
  }

exit:
  if(aerosol_signatures) MEM_RM(atm->allocator, aerosol_signatures);
  return res;
error:
  log_err(atm, "mesh signature calculation error -- %s\n", res_to_cstr(res));
  goto exit;
}

static res_T
compute_aerosol_sars_signature
  (const struct aerosol* aerosol,
   const double range[2], /* In nm. Limits are inclusive */
   hash256_T hash)
{
  size_t ibands[2];
  res_T res = RES_OK;
  ASSERT(aerosol && range && hash && range[0] < range[1]);

  res = sars_find_bands(aerosol->sars, range, ibands);
  if(res != RES_OK) goto error;

  /* No band are covered by the spectral range */
  if(ibands[0] > ibands[1]) {
    memset(hash, 0, sizeof(hash256_T));

  /* Hash the data of the covered aerosol bands */
  } else {
    struct sha256_ctx ctx;
    size_t iband;

    sha256_ctx_init(&ctx);
    FOR_EACH(iband, ibands[0], ibands[1]+1) {
      res = sars_band_compute_hash(aerosol->sars, iband, hash);
      if(res != RES_OK) goto error;
      sha256_ctx_update(&ctx, hash, sizeof(hash256_T));
    }
    sha256_ctx_finalize(&ctx, hash);
  }

exit:
  return res;
error:
  goto exit;
}

static res_T
compute_band_signature
  (const struct rnatm* atm,
   const size_t iband,
   hash256_T band_signature)
{
  hash256_T gas_signature;
  hash256_T* aerosol_signatures = NULL;
  size_t naerosols = 0;
  size_t i;
  res_T res = RES_OK;
  ASSERT(atm && band_signature);

  /* Compute the signature for the gas */
  res = sck_band_compute_hash(atm->gas.ck, iband, gas_signature);
  if(res != RES_OK) goto error;

  naerosols = darray_aerosol_size_get(&atm->aerosols);

  if(!naerosols) {
    memcpy(band_signature, gas_signature, sizeof(hash256_T));
  } else {
    struct sha256_ctx ctx;
    struct sck_band band = SCK_BAND_NULL;
    double range[2];

    aerosol_signatures = MEM_CALLOC(atm->allocator, naerosols, sizeof(hash256_T));
    if(!aerosol_signatures) { res = RES_MEM_ERR; goto error; }

    /* Retrieve the spectral range of the band */
    SCK(get_band(atm->gas.ck, iband, &band));
    range[0] = band.lower;
    range[1] = nextafter(band.upper, DBL_MAX); /* Make limit inclusive */

    FOR_EACH(i, 0, naerosols) {
      const struct aerosol* aerosol = darray_aerosol_cdata_get(&atm->aerosols)+i;

      res = compute_aerosol_sars_signature(aerosol, range, aerosol_signatures[i]);
      if(res != RES_OK) goto error;
    }

    /* Pay attention to the order of how aerosol hashes are registered.
     * Therefore, the same set of aerosols will have the same signature (i.e. the
     * same hash), regardless of the order in which the aerosols were listed in
     * the input arguments */
    qsort(aerosol_signatures, naerosols, sizeof(hash256_T), cmp_hash256);

    /* Build the signature for the dataset */
    sha256_ctx_init(&ctx);
    sha256_ctx_update(&ctx, gas_signature, sizeof(hash256_T));
    FOR_EACH(i, 0, naerosols) {
      sha256_ctx_update(&ctx, aerosol_signatures[i], sizeof(hash256_T));
    }
    sha256_ctx_finalize(&ctx, band_signature);
  }

exit:
  if(aerosol_signatures) MEM_RM(atm->allocator, aerosol_signatures);
  return res;
error:
  log_err(atm, "signature calculation error for the band %lu -- %s\n",
    (unsigned long)iband, res_to_cstr(res));
  goto exit;
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
res_T
octrees_storage_desc_init
  (const struct rnatm* atm,
   struct octrees_storage_desc* desc)
{
  size_t iband;
  size_t nbands;
  res_T res = RES_OK;
  ASSERT(atm && desc);

  *desc = OCTREES_STORAGE_DESC_NULL;

  desc->version = OCTREES_STORAGE_VERSION;
  desc->ibands[0] = atm->ibands[0];
  desc->ibands[1] = atm->ibands[1];
  desc->optical_thickness = atm->optical_thickness;
  desc->grid_definition[0] = atm->grid_definition[0];
  desc->grid_definition[1] = atm->grid_definition[1];
  desc->grid_definition[2] = atm->grid_definition[2];
  desc->allocator = atm->allocator;

  log_info(atm, "sign octree data\n");

  /* Calculate the signature of volumetric meshes */
  res = compute_mesh_signature(atm, desc->mesh_signature);
  if(res != RES_OK) goto error;

  /* Allocate the list of band signatures */
  nbands = atm->ibands[1] - atm->ibands[0] + 1;
  desc->band_signatures = MEM_CALLOC(desc->allocator, nbands, sizeof(hash256_T));
  if(!desc->band_signatures) {
    log_err(atm, "error allocating signatures per band\n");
    res = RES_MEM_ERR;
    goto error;
  }

  /* Calculate the signature of spectral bands */
  FOR_EACH(iband, atm->ibands[0], atm->ibands[1]+1) {
    const size_t i = iband - atm->ibands[0];
    res = compute_band_signature(atm, iband, desc->band_signatures[i]);
    if(res != RES_OK) goto error;
  }

exit:
  return res;
error:
  goto exit;
}

res_T
octrees_storage_desc_init_from_stream
  (const struct rnatm* atm,
   struct octrees_storage_desc* desc,
   FILE* stream)
{
  size_t iband;
  size_t nbands;
  res_T res = RES_OK;
  ASSERT(atm && desc && stream);

  *desc = OCTREES_STORAGE_DESC_NULL;

  desc->allocator = atm->allocator;

  #define READ(Var, N) {                                                       \
    if(fread((Var), sizeof(*(Var)), (N), stream) != (N)) {                     \
      if(feof(stream)) {                                                       \
        res = RES_BAD_ARG;                                                     \
      } else if(ferror(stream)) {                                              \
        res = RES_IO_ERR;                                                      \
      } else {                                                                 \
        res = RES_UNKNOWN_ERR;                                                 \
      }                                                                        \
      goto error;                                                              \
    }                                                                          \
  } (void)0
  READ(&desc->version, 1);
  if(desc->version != OCTREES_STORAGE_VERSION) { /* Basic versioning */
    res = RES_BAD_ARG;
    goto error;
  }
  READ(desc->ibands, 2);
  READ(&desc->optical_thickness, 1);
  READ(desc->grid_definition, 3);
  READ(desc->mesh_signature, sizeof(hash256_T));

  /* Allocate the list of band signatures */
  nbands = desc->ibands[1] - desc->ibands[0] + 1;
  desc->band_signatures = MEM_CALLOC(desc->allocator, nbands, sizeof(hash256_T));
  if(!desc->band_signatures) {
    log_err(atm, "error allocating signatures per band\n");
    res = RES_MEM_ERR;
    goto error;
  }

  FOR_EACH(iband, 0, nbands) {
    READ(desc->band_signatures[iband], sizeof(hash256_T));
  }
  #undef READ

exit:
  return res;
error:
  log_err(atm, "unable to read tree storage descriptor -- %s\n",
    res_to_cstr(res));
  goto exit;
}

void
octrees_storage_desc_release(struct octrees_storage_desc* desc)
{
  ASSERT(desc);
  if(desc->band_signatures) MEM_RM(desc->allocator, desc->band_signatures);
}

res_T
write_octrees_storage_desc
  (const struct rnatm* atm,
   const struct octrees_storage_desc* desc,
   FILE* stream)
{
  size_t iband;
  size_t nbands;
  res_T res = RES_OK;
  ASSERT(atm && desc && stream);

  nbands = desc->ibands[1] - desc->ibands[0] + 1;

  #define WRITE(Var, N) {                                                      \
    if(fwrite((Var), sizeof(*(Var)), (N), stream) != (N)) {                    \
      res = RES_IO_ERR;                                                        \
      goto error;                                                              \
    }                                                                          \
  } (void)0
  WRITE(&desc->version, 1);
  WRITE(desc->ibands, 2);
  WRITE(&desc->optical_thickness, 1);
  WRITE(desc->grid_definition, 3);
  WRITE(desc->mesh_signature, sizeof(hash256_T));
  FOR_EACH(iband, 0, nbands) {
    WRITE(desc->band_signatures[iband], sizeof(hash256_T));
  }
  #undef WRITE

exit:
  return res;
error:
  log_err(atm, "unable to write tree storage descriptor\n");
  goto exit;
}

res_T
check_octrees_storage_compatibility
  (const struct octrees_storage_desc* reference,
   const struct octrees_storage_desc* challenge)
{
  size_t iband_challenge;
  ASSERT(reference && challenge);

  /* Check version equality */
  if(reference->version != challenge->version)
    return RES_BAD_ARG;

  /* Verify that the bands in the repository to be challenged are included in
   * the bands in the reference repository */
  if(reference->ibands[0] > challenge->ibands[0]
  || reference->ibands[1] < challenge->ibands[1])
    return RES_BAD_ARG;

  /* Check the equality of optical thicknesses */
  if(reference->optical_thickness != challenge->optical_thickness)
    return RES_BAD_ARG;

  /* Check the equality of grid definitions */
  if(reference->grid_definition[0] != challenge->grid_definition[0]
  || reference->grid_definition[1] != challenge->grid_definition[1]
  || reference->grid_definition[2] != challenge->grid_definition[2])
    return RES_BAD_ARG;

  /* Check the equality of meshes */
  if(!hash256_eq(reference->mesh_signature, challenge->mesh_signature))
    return RES_BAD_ARG;

  /* Check the equality of the per band data */
  FOR_EACH(iband_challenge, challenge->ibands[0], challenge->ibands[1]+1) {
    const size_t ichallenge = iband_challenge - challenge->ibands[0];
    const size_t ireference = iband_challenge - reference->ibands[0];
    hash256_T* band_challenge = challenge->band_signatures+ichallenge;
    hash256_T* band_reference = reference->band_signatures+ireference;
    if(!hash256_eq(*band_challenge, *band_reference))
       return RES_BAD_ARG;
  }

  return RES_OK;
}

res_T
reserve_octrees_storage_toc(const struct rnatm* atm, FILE* stream)
{
  size_t noctrees = 0;
  size_t sizeof_toc = 0;
  int err;
  res_T res = RES_OK;
  ASSERT(atm && stream);

  noctrees = darray_accel_struct_size_get(&atm->accel_structs);

  /* Compute the size of TOC */
  sizeof_toc =
    sizeof(size_t)/*#octrees*/
  + noctrees * (sizeof(size_t)/*iband*/+sizeof(size_t)/*iquad*/+sizeof(fpos_t));
  ASSERT(sizeof_toc < LONG_MAX);

  /* Reserve the space for the table of contents */
  err = fseek(stream, (long)sizeof_toc, SEEK_CUR);
  if(err != 0) { res = RES_IO_ERR; goto error; }

exit:
  return res;
error:
  log_err(atm, "error reserving octree storage toc -- %s\n",
    strerror(errno));
  goto exit;
}

res_T
write_octrees_storage_toc(const struct rnatm* atm, FILE* stream)
{
  size_t noctrees = 0;
  size_t ioctree = 0;
  res_T res = RES_OK;
  ASSERT(atm && stream);

  noctrees = darray_accel_struct_size_get(&atm->accel_structs);

  /* Write the number of voxelized octrees and their corresponding offset */
  #define WRITE(Var) {                                                         \
    if(fwrite((Var), sizeof(*(Var)), 1, stream) != 1) {                        \
      res = RES_IO_ERR;                                                        \
      goto error;                                                              \
    }                                                                          \
  } (void)0
  WRITE(&noctrees);
  FOR_EACH(ioctree, 0, noctrees) {
    const struct accel_struct* accel_struct =
      darray_accel_struct_cdata_get(&atm->accel_structs) + ioctree;
    WRITE(&accel_struct->iband);
    WRITE(&accel_struct->iquad_pt);
    WRITE(&accel_struct->fpos);
  }
  #undef WRITE

exit:
  return res;
error:
  log_err(atm, "error writing octree storage table of content -- %s\n",
    res_to_cstr(res));
  goto exit;
}

res_T
read_octrees_storage_toc(struct rnatm* atm, FILE* stream)
{
  struct accel_struct* accel_structs = NULL;
  size_t ioctree = 0;
  size_t noctrees = 0;
  size_t ioctree_stream = 0;
  size_t noctrees_stream = 0;
  size_t iband = 0;
  size_t iquad = 0;
  res_T res = RES_OK;
  ASSERT(atm && stream);

  noctrees = darray_accel_struct_size_get(&atm->accel_structs);

  #define READ(Var) {                                                          \
    if(fread((Var), sizeof(*(Var)), (1), stream) != (1)) {                     \
      if(feof(stream)) {                                                       \
        res = RES_BAD_ARG;                                                     \
      } else if(ferror(stream)) {                                              \
        res = RES_IO_ERR;                                                      \
      } else {                                                                 \
        res = RES_UNKNOWN_ERR;                                                 \
      }                                                                        \
      goto error;                                                              \
    }                                                                          \
  } (void)0

  accel_structs = darray_accel_struct_data_get(&atm->accel_structs);
  noctrees = darray_accel_struct_size_get(&atm->accel_structs);

  /* Read the number of octrees stored in the stream */
  READ(&noctrees_stream);
  if(noctrees_stream < noctrees) {
    res = RES_BAD_ARG;
    goto error;
  }

  /* Look for the first octree to load for the current atmosphere */
  FOR_EACH(ioctree_stream, 0, noctrees_stream) {
    READ(&iband);
    READ(&iquad);
    READ(&accel_structs[0].fpos);

    if(accel_structs[0].iband == iband
    && accel_structs[0].iquad_pt == iquad)
      break;
  }

  /* Cannot find the first octree */
  if(ioctree_stream >= noctrees_stream) {
    res = RES_BAD_ARG;
    goto error;
  }

  /* Load the remaining offsets */
  FOR_EACH(ioctree, 1, noctrees) {

    /* No sufficient octrees in the stream */
    if(++ioctree_stream >= noctrees_stream) {
      res = RES_BAD_ARG;
      goto error;
    }

    READ(&iband);
    READ(&iquad);
    READ(&accel_structs[ioctree].fpos);

    /* Unexpected octrees */
    if(accel_structs[ioctree].iband != iband
    || accel_structs[ioctree].iquad_pt != iquad) {
      res = RES_BAD_ARG;
      goto error;
    }
  }
  #undef READ

exit:
  return res;
error:
  log_err(atm, "error reading octree storage table of content -- %s\n",
    res_to_cstr(res));
  goto exit;
}

extern LOCAL_SYM res_T
store_octrees(struct rnatm* atm, const size_t ioctrees[2], FILE* stream)
{
  size_t ioctree;
  int err;
  res_T res = RES_OK;
  ASSERT(atm && ioctrees && stream && ioctrees[0] <= ioctrees[1]);

  FOR_EACH(ioctree, ioctrees[0], ioctrees[1]+1) {
    struct accel_struct* accel_struct = NULL;
    accel_struct = darray_accel_struct_data_get(&atm->accel_structs) + ioctree;

    /* Save the current file offset */
    err = fgetpos(stream, &accel_struct->fpos);
    if(err != 0) {
      log_err(atm, "error retrieving octree storage position -- %s\n",
        strerror(errno));
      res = RES_IO_ERR;
      goto error;
    }

    /* Serialize the octree */
    res = svx_tree_write(accel_struct->octree, stream);
    if(res != RES_OK) {
      log_err(atm, "error writing octree %lu -- %s\n",
        (unsigned long)ioctree, res_to_cstr(res));
      goto error;
    }
  }

exit:
  return res;
error:
  goto exit;
}
