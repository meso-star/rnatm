/* Copyright (C) 2022, 2023 Centre National de la Recherche Scientifique
 * Copyright (C) 2022, 2023 Institut Pierre-Simon Laplace
 * Copyright (C) 2022, 2023 Institut de Physique du Globe de Paris
 * Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2022, 2023 Observatoire de Paris
 * Copyright (C) 2022, 2023 Université de Reims Champagne-Ardenne
 * Copyright (C) 2022, 2023 Université de Versaille Saint-Quentin
 * Copyright (C) 2022, 2023 Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L /* nextafter */

#include "rnatm_c.h"

#include <star/sars.h>
#include <star/sck.h>

#include <rsys/float4.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static int
tetra_get_radcoef_gas
  (const struct rnatm* atm,
   const struct suvm_primitive* tetra,
   const size_t iband,
   const size_t iquad_pt,
   const enum rnatm_radcoef radcoef,
   float k[4])
{
  struct sck_band band;
  float ka[4];
  float ks[4];
  ASSERT(atm && tetra && k);
  ASSERT(iband < sck_get_bands_count(atm->gas.ck));
  ASSERT(!SUVM_PRIMITIVE_NONE(tetra));
  ASSERT(tetra->nvertices == 4);
  ASSERT(tetra->indices[0] < sck_get_nodes_count(atm->gas.ck));
  ASSERT(tetra->indices[1] < sck_get_nodes_count(atm->gas.ck));
  ASSERT(tetra->indices[2] < sck_get_nodes_count(atm->gas.ck));
  ASSERT(tetra->indices[3] < sck_get_nodes_count(atm->gas.ck));

  /* Retrieve the band */
  SCK(get_band(atm->gas.ck, iband, &band));
  ASSERT(iquad_pt < band.quad_pts_count);

  /* Get the absorption coefficients */
  if(radcoef == RNATM_RADCOEF_Ka || radcoef == RNATM_RADCOEF_Kext) {
    /* Retrieve the quadrature point */
    struct sck_quad_pt quad;
    SCK(band_get_quad_pt(&band, iquad_pt, &quad));
    ka[0] = quad.ka_list[tetra->indices[0]];
    ka[1] = quad.ka_list[tetra->indices[1]];
    ka[2] = quad.ka_list[tetra->indices[2]];
    ka[3] = quad.ka_list[tetra->indices[3]];
  }

  /* Get the scattering coefficients */
  if(radcoef == RNATM_RADCOEF_Ks || radcoef == RNATM_RADCOEF_Kext) {
    ks[0] = band.ks_list[tetra->indices[0]];
    ks[1] = band.ks_list[tetra->indices[1]];
    ks[2] = band.ks_list[tetra->indices[2]];
    ks[3] = band.ks_list[tetra->indices[3]];
  }

  switch(radcoef) {
    case RNATM_RADCOEF_Ka: f4_set(k, ka); break;
    case RNATM_RADCOEF_Ks: f4_set(k, ks); break;
    case RNATM_RADCOEF_Kext: f4_add(k, ka, ks); break;
    default: FATAL("Unreachable code\n");
  }

  return 1;
}

static int
tetra_get_radcoef_aerosol
  (const struct rnatm* atm,
   const struct suvm_primitive* tetra,
   const size_t iband,
   const struct aerosol* aerosol,
   const enum rnatm_radcoef radcoef,
   float k[4])
{
  struct sck_band gas_band;
  struct sars_band ars_band;
  double gas_spectral_range[2]; /* In nm */
  size_t ars_ibands[2];
  float ka[4], ks[4];
  ASSERT(atm && aerosol && tetra && k);

  ASSERT(!SUVM_PRIMITIVE_NONE(tetra));
  ASSERT(tetra->nvertices == 4);
  ASSERT(tetra->indices[0] < sars_get_nodes_count(aerosol->sars));
  ASSERT(tetra->indices[1] < sars_get_nodes_count(aerosol->sars));
  ASSERT(tetra->indices[2] < sars_get_nodes_count(aerosol->sars));
  ASSERT(tetra->indices[3] < sars_get_nodes_count(aerosol->sars));

  /* Look for the aerosol bands covered by the gas band */
  SCK(get_band(atm->gas.ck, iband, &gas_band));
  gas_spectral_range[0] = gas_band.lower;
  gas_spectral_range[1] = nextafter(gas_band.upper, 0); /* inclusive */
  SARS(find_bands(aerosol->sars, gas_spectral_range, ars_ibands));

  /* No aerosol band is overlaid by the gas band */
  if(ars_ibands[0] > ars_ibands[1]) {
    k[0] = 0;
    k[1] = 0;
    k[2] = 0;
    k[3] = 0;
    return 0;
  }

  SARS(get_band(aerosol->sars, ars_ibands[0], &ars_band));

  /* The gas band is included in an aerosol band: use the aerosol radiative
   * coefficients */
  if(ars_ibands[0] == ars_ibands[1]
  && ars_band.lower <= gas_band.lower
  && ars_band.upper >= gas_band.upper) {

    /* Get the absorption coefficient  */
    if(radcoef == RNATM_RADCOEF_Ka || radcoef == RNATM_RADCOEF_Kext) {
      ka[0] = sars_band_get_ka(&ars_band, tetra->indices[0]);
      ka[1] = sars_band_get_ka(&ars_band, tetra->indices[1]);
      ka[2] = sars_band_get_ka(&ars_band, tetra->indices[2]);
      ka[3] = sars_band_get_ka(&ars_band, tetra->indices[3]);
    }

    /* Get the scattering coefficient  */
    if(radcoef == RNATM_RADCOEF_Ks || radcoef == RNATM_RADCOEF_Kext) {
      ks[0] = sars_band_get_ks(&ars_band, tetra->indices[0]);
      ks[1] = sars_band_get_ks(&ars_band, tetra->indices[1]);
      ks[2] = sars_band_get_ks(&ars_band, tetra->indices[2]);
      ks[3] = sars_band_get_ks(&ars_band, tetra->indices[3]);
    }

  /* The gas band overlaid N aerosol bands (N >= 1) */
  } else {
    float tau_ka[4] = {0, 0, 0, 0};
    float tau_ks[4] = {0, 0, 0, 0};
    double lambda_min;
    double lambda_max;
    float rcp_gas_band_len;
    size_t iars_band;

    FOR_EACH(iars_band, ars_ibands[0], ars_ibands[1]+1) {
      float lambda_len;

      SARS(get_band(aerosol->sars, iars_band, &ars_band));
      lambda_min = MMAX(gas_band.lower, ars_band.lower);
      lambda_max = MMIN(gas_band.upper, ars_band.upper); /* exclusive */
      lambda_max = nextafter(lambda_max, 0); /* inclusive */
      lambda_len = (float)(lambda_max - lambda_min);
      ASSERT(lambda_len > 0);

      /* Get the absorption coefficient  */
      if(radcoef == RNATM_RADCOEF_Ka || radcoef == RNATM_RADCOEF_Kext) {
        tau_ka[0] += sars_band_get_ka(&ars_band, tetra->indices[0]) * lambda_len;
        tau_ka[1] += sars_band_get_ka(&ars_band, tetra->indices[1]) * lambda_len;
        tau_ka[2] += sars_band_get_ka(&ars_band, tetra->indices[2]) * lambda_len;
        tau_ka[3] += sars_band_get_ka(&ars_band, tetra->indices[3]) * lambda_len;
      }

      /* Get the scattering coefficient  */
      if(radcoef == RNATM_RADCOEF_Ks || radcoef == RNATM_RADCOEF_Kext) {
        tau_ks[0] += sars_band_get_ks(&ars_band, tetra->indices[0]) * lambda_len;
        tau_ks[1] += sars_band_get_ks(&ars_band, tetra->indices[1]) * lambda_len;
        tau_ks[2] += sars_band_get_ks(&ars_band, tetra->indices[2]) * lambda_len;
        tau_ks[3] += sars_band_get_ks(&ars_band, tetra->indices[3]) * lambda_len;
      }
    }

    /* Compute the radiative coefficients of the tetrahedron */
    lambda_min = gas_band.lower;
    lambda_max = nextafter(gas_band.upper, 0); /* inclusive */
    rcp_gas_band_len = 1.f/(float)(lambda_max - lambda_min);
    f4_mulf(ks, tau_ks, rcp_gas_band_len);
    f4_mulf(ka, tau_ka, rcp_gas_band_len);
  }

  switch(radcoef) {
    case RNATM_RADCOEF_Ka: f4_set(k, ka); break;
    case RNATM_RADCOEF_Ks: f4_set(k, ks); break;
    case RNATM_RADCOEF_Kext: f4_add(k, ka, ks); break;
    default: FATAL("Unreachable code\n");
  }

  return 1;
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
int
tetra_get_radcoef
  (const struct rnatm* atm,
   const struct suvm_primitive* tetra,
   const size_t component,
   const size_t iband,
   const size_t iquad_pt,
   const enum rnatm_radcoef radcoef,
   float k[4])
{
  ASSERT(atm);

  if(SUVM_PRIMITIVE_NONE(tetra)) {
    k[0] = 0;
    k[1] = 0;
    k[2] = 0;
    k[3] = 0;
    return 0;
  }

  if(component == RNATM_GAS) {
    return tetra_get_radcoef_gas(atm, tetra, iband, iquad_pt, radcoef, k);
  } else {
    const struct aerosol* aerosol = NULL;
    ASSERT(component < darray_aerosol_size_get(&atm->aerosols));
    aerosol = darray_aerosol_cdata_get(&atm->aerosols) + component;
    return tetra_get_radcoef_aerosol(atm, tetra, iband, aerosol, radcoef, k);
  }
}
