# Rad-Net ATMosphere

This C library loads and manages data describing an atmosphere.

## Requirements

- C compiler with OpenMP support
- POSIX make
- pkg-config
- [Rad-Net Scattering Functions](https://gitlab.com/meso-star/rnsf)
- [Rad-Net String List](https://gitlab.com/meso-star/rnsl)
- [RSys](https://gitlab.com/vaplv/rsys)
- [Star AeRoSol](https://gitlab.com/meso-star/star-aerosol)
- [Star Buffer](https://gitlab.com/meso-star/star-buffer)
- [Star CorrelatedK](https://gitlab.com/meso-star/star-ck)
- [Star Mesh](https://gitlab.com/meso-star/star-mesh)
- [Star Scattering Functions](https://gitlab.com/meso-star/star-sf)
- [Star Unstructured Volumetric Mesh](https://gitlab.com/meso-star/star-uvm)
- [Star VoXel](https://gitlab.com/meso-star/star-vx)

## Installation

Edit config.mk as needed, then run:

    make clean install

## Release notes

###  Version 0.1

- Fix voxelization deadlock.
- Fix of a bug when calculating the octree definition: the definition
  should be equal to 0, which would result in an error.
- Fix the calculation of radiative coefficients. They were erroneous
  when the gas band overlapped several aerosol bands: the aerosol
  average was wrongly reduced to the integration domain.
- Write the man pages directly in mdoc's roff macros, instead of using
  the scdoc markup language as a source for man pages.
- Replace CMake by Makefile as build system.
- Update compiler and linker flags to increase the security and
  robustness of generated binaries.
- Provide a pkg-config file to link the library as an external
  dependency.

### Version 0.0.1

Fix several bugs when volumetric meshes of the components are not
identical:

- Fix the `rnatm_sample_component` function. The cumulative was
  calculated assuming that all components are either present or absent
  at the queried position. Thus, the accumulation was only correct when
  the volumetric meshes were the same for all components.
- Fix the calculation of the minimum radiative coefficient of a voxel in
  the octree. It was simply miscalculated and its value might not be the
  minimum.
- Fix the accumulation of radiative coefficients in the voxels of the
  octree.  When the volumetric meshes were not the same for all
  components, some voxels could be "emptied" during the accumulation.

## Copyrights

Copyright (C) 2022, 2023 Centre National de la Recherche Scientifique  
Copyright (C) 2022, 2023 Institut Pierre-Simon Laplace  
Copyright (C) 2022, 2023 Institut de Physique du Globe de Paris  
Copyright (C) 2022, 2023 [|Méso|Star>](https://www.meso-star.com) (contact@meso-star.com)  
Copyright (C) 2022, 2023 Observatoire de Paris  
Copyright (C) 2022, 2023 Université de Reims Champagne-Ardenne  
Copyright (C) 2022, 2023 Université de Versaille Saint-Quentin  
Copyright (C) 2022, 2023 Université Paul Sabatier

## License

Rad-Net Atmosphere is free software released under the GPL v3+ license:
GNU GPL version 3 or later. You are welcome to redistribute it under
certain conditions; refer to the COPYING file for details.
