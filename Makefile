# Copyright (C) 2022, 2023 Centre National de la Recherche Scientifique
# Copyright (C) 2022, 2023 Institut Pierre-Simon Laplace
# Copyright (C) 2022, 2023 Institut de Physique du Globe de Paris
# Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
# Copyright (C) 2022, 2023 Observatoire de Paris
# Copyright (C) 2022, 2023 Université de Reims Champagne-Ardenne
# Copyright (C) 2022, 2023 Université de Versaille Saint-Quentin
# Copyright (C) 2022, 2023 Université Paul Sabatier
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

.POSIX:
.SUFFIXES: # Clean up default inference rules

include config.mk

LIBNAME_STATIC = librnatm.a
LIBNAME_SHARED = librnatm.so
LIBNAME = $(LIBNAME_$(LIB_TYPE))

################################################################################
# Library building
################################################################################
SRC =\
 src/rnatm.c\
 src/rnatm_log.c\
 src/rnatm_mesh.c\
 src/rnatm_octree.c\
 src/rnatm_octrees_storage.c\
 src/rnatm_properties.c\
 src/rnatm_radcoef.c\
 src/rnatm_voxel_partition.c\
 src/rnatm_write_vtk.c
OBJ = $(SRC:.c=.o)
DEP = $(SRC:.c=.d)

build_library: .config $(DEP)
	@$(MAKE) -fMakefile $$(for i in $(DEP); do echo -f $${i}; done) \
	$$(if [ -n "$(LIBNAME)" ]; then \
	     echo "$(LIBNAME)"; \
	   else \
	     echo "$(LIBNAME_SHARED)"; \
	   fi)

$(DEP) $(OBJ): config.mk

$(LIBNAME_SHARED): $(OBJ)
	$(CC) $(CFLAGS_SO) $(DPDC_CFLAGS) -o $@ $(OBJ) $(LDFLAGS_SO) $(DPDC_LIBS)

$(LIBNAME_STATIC): librnatm.o
	$(AR) -rc $@ $?
	$(RANLIB) $@

librnatm.o: $(OBJ)
	$(LD) -r $(OBJ) -o $@
	$(OBJCOPY) $(OCPFLAGS) $@

.config: config.mk
	@if ! $(PKG_CONFIG) --atleast-version $(RNSF_VERSION) rnsf; then \
	  echo "rnsf $(RNSF_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(RNSL_VERSION) rnsl; then \
	  echo "rnsl $(RNSL_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(RSYS_VERSION) rsys; then \
	  echo "rsys $(RSYS_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(SARS_VERSION) sars; then \
	  echo "sars $(SARS_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(SBUF_VERSION) sbuf; then \
	  echo "sbuf $(SBUF_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(SCK_VERSION) sck; then \
	  echo "sck $(SCK_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(SMSH_VERSION) smsh; then \
	  echo "smsh $(SMSH_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(SSF_VERSION) ssf; then \
	  echo "ssf $(SSF_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(SUVM_VERSION) suvm; then \
	  echo "suvm $(SUVM_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(SVX_VERSION) svx; then \
	  echo "svx $(SVX_VERSION) not found" >&2; exit 1; fi
	@echo "config done" > $@

.SUFFIXES: .c .d .o
.c.d:
	@$(CC) $(CFLAGS_SO) $(DPDC_CFLAGS) -MM -MT "$(@:.d=.o) $@" $< -MF $@

.c.o:
	$(CC) $(CFLAGS_SO) $(DPDC_CFLAGS) -DRNATM_SHARED_BUILD -c $< -o $@

################################################################################
# Installation
################################################################################
pkg:
	sed -e 's#@PREFIX@#$(PREFIX)#g'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@RNSF_VERSION@#$(RNSF_VERSION)#g'\
	    -e 's#@RNSL_VERSION@#$(RNSL_VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g'\
	    -e 's#@SARS_VERSION@#$(SARS_VERSION)#g'\
	    -e 's#@SBUF_VERSION@#$(SBUF_VERSION)#g'\
	    -e 's#@SCK_VERSION@#$(SCK_VERSION)#g'\
	    -e 's#@SMSH_VERSION@#$(SMSH_VERSION)#g'\
	    -e 's#@SSF_VERSION@#$(SSF_VERSION)#g'\
	    -e 's#@SUVM_VERSION@#$(SUVM_VERSION)#g'\
	    -e 's#@SVX_VERSION@#$(SVX_VERSION)#g'\
	    rnatm.pc.in > rnatm.pc

rnatm-local.pc: rnatm.pc.in
	sed -e '1d'\
	    -e 's#^includedir=.*#includedir=./src/#'\
	    -e 's#^libdir=.*#libdir=./#'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@RNSF_VERSION@#$(RNSF_VERSION)#g'\
	    -e 's#@RNSL_VERSION@#$(RNSL_VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g'\
	    -e 's#@SARS_VERSION@#$(SARS_VERSION)#g'\
	    -e 's#@SBUF_VERSION@#$(SBUF_VERSION)#g'\
	    -e 's#@SCK_VERSION@#$(SCK_VERSION)#g'\
	    -e 's#@SMSH_VERSION@#$(SMSH_VERSION)#g'\
	    -e 's#@SSF_VERSION@#$(SSF_VERSION)#g'\
	    -e 's#@SUVM_VERSION@#$(SUVM_VERSION)#g'\
	    -e 's#@SVX_VERSION@#$(SVX_VERSION)#g'\
	    rnatm.pc.in > $@

install: build_library pkg
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib" $(LIBNAME)
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib/pkgconfig" rnatm.pc
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/include/rad-net" src/rnatm.h
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/share/doc/rnatm" COPYING README.md
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/share/man/man5" rngt.5 rnpfi.5

uninstall:
	rm -f "$(DESTDIR)$(PREFIX)/lib/$(LIBNAME)"
	rm -f "$(DESTDIR)$(PREFIX)/lib/pkgconfig/rnatm.pc"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/rnatm/COPYING"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/rnatm/README.md"
	rm -f "$(DESTDIR)$(PREFIX)/include/rad-net/rnatm.h"
	rm -f "$(DESTDIR)$(PREFIX)/share/man/man5/rngt.5"
	rm -f "$(DESTDIR)$(PREFIX)/share/man/man5/rnpfi.5"

################################################################################
# Miscellaneous targets
################################################################################
all: build_library build_tests

clean: clean_test
	rm -f $(OBJ) $(LIBNAME)
	rm -f .config librnatm.o rnatm.pc rnatm-local.pc

distclean: clean
	rm -f $(DEP) src/test_rnatm.d

lint:
	shellcheck -o all make.sh
	mandoc -Tlint -Wall rngt.5 || [ $$? -le 1 ]
	mandoc -Tlint -Wall rnpfi.5 || [ $$? -le 1 ]

################################################################################
# Tests
################################################################################
TEST_SRC = src/test_rnatm.c
TEST_OBJ = $(TEST_SRC:.c=.o)
TEST_DEP = $(TEST_SRC:.c=.d)

PKG_CONFIG_LOCAL = PKG_CONFIG_PATH="./:$${PKG_CONFIG_PATH}" $(PKG_CONFIG)
RNATM_CFLAGS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --cflags rnatm-local.pc)
RNATM_LIBS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --libs rnatm-local.pc)

build_tests: build_library src/test_rnatm.d
	@$(MAKE) -fMakefile -f src/test_rnatm.d test_rnatm

clean_test:
	rm -f test_rnatm src/test_rnatm.o

$(TEST_DEP): config.mk rnatm-local.pc
	@$(CC) $(CFLAGS_EXE) $(RNATM_CFLAGS) $(RSYS_CFLAGS) \
	-MM -MT "$(@:.d=.o) $@" $(@:.d=.c) -MF $@

src/test_rnatm.o: config.mk rnatm-local.pc
	$(CC) $(CFLAGS_EXE) $(RNATM_CFLAGS) $(RSYS_CFLAGS) -c $(@:.o=.c) -o $@

test_rnatm: src/test_rnatm.o config.mk rnatm-local.pc $(LIBNAME)
	$(CC) $(CFLAGS_EXE) $(RNATM_CFLAGS) $(RSYS_CFLAGS) -o $@ src/$@.o \
	$(LDFLAGS_EXE) $(RNATM_LIBS) $(RSYS_LIBS)
